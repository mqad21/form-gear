import { createContext, createEffect, useContext } from "solid-js";
import { createStore } from "solid-js/store";
import { locale } from "./stores/LocaleStore";

type FormState = {
	activeComponent: ActiveComponent,
	alert: Alert
}

type ActiveComponent = {
	dataKey: string
	label: string
	index: number[]
	position: number
}

type AlertContent = {
	title: string,
	text: string,
	button: {
		yes: string,
		no: string
	},
}

type Alert = {
	isShown: boolean,
	callback: () => void,
} & AlertContent

type FormAction = {
	setActiveComponent?: (newComponent: ActiveComponent) => void,
	showAlert?: (content: AlertContent, callback: () => void) => void,
	hideAlert?: () => void
}

type FormStore = [
	FormState,
	FormAction
]

const FormContext = createContext<FormStore>();

export function FormProvider(props) {
	const [form, setState] = createStore<FormState>({
		activeComponent: { dataKey: '', label: '', index: [], position: 0 },
		alert: {
			isShown: false,
			title: '',
			text: '',
			button: {
				yes: locale.details.language[0].buttonYes,
				no: locale.details.language[0].buttonNo
			},
			callback: () => { }
		}
	})

	let store: FormStore = [
		form,
		{
			setActiveComponent(component) {
				setState("activeComponent", component);
			},
			showAlert(content, callback) {
				setState("alert", {
					button: content.button,
					title: content.title,
					text: content.text,
					isShown: true,
					callback
				})
				// setState("alert", "title", content.title)
				// setState("alert", "text", content.text)
				// setState("alert", "button", content.button)
				// setState("alert", "isShown", true)
			},
			hideAlert() {
				setState("alert", "isShown", false)
			}
		}
	];

	return (
		<FormContext.Provider value={store}>
			{props.children}
		</FormContext.Provider>
	)
}

export function useForm() {
	return useContext(FormContext);
}