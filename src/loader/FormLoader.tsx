import { mergeProps, onCleanup, onMount } from "solid-js";
import Loader from "../assets/loader.gif"
// import { useLoaderDispatch } from "./FormLoaderProvider";

export default function FormLoader(props) {

    // const { removeLoader } = useLoaderDispatch();
    let merged = mergeProps({ type: "success", autoHideDuration: 70 }, props);
    let timerRef

    onMount(() => {
        timerRef = setTimeout(() => merged.remove(), merged.autoHideDuration)
    })

    onCleanup(() => {
        clearTimeout(timerRef)
    })

    return (
        <div class="backdrop-blur-sm overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none justify-center items-center flex">
            <img src={Loader} width="128" />
        </div>
    );
}

