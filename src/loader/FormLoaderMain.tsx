import { counter } from "../stores/CounterStore";
import Loader from "../assets/loader.gif"
import { Show } from "solid-js";

export default function FormLoader1() {

    return (
        <div class="overflow-hidden">
            <div class="bg-gray-50 dark:bg-gray-900 dark:text-white h-screen shadow-xl text-gray-600 flex overflow-hidden text-sm font-montserrat rounded-lg  dark:shadow-gray-800">

                <div class=" flex-grow flex flex-col bg-white dark:bg-gray-900 z-0">

                    <div class="relative md:flex max-h-screen  ">
                        <div class="block">
                            <div
                                class="backdrop-blur-sm overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none grid grid-cols-5 place-items-stretch">
                                <div class="lg:col-span-2"></div>
                                <div class="col-span-3 lg:col-span-1 my-auto">
                                    <Show when={counter.renderState} fallback={
                                        <img class="mx-auto" src={Loader} width="128" alt="loader" />
                                    }>
                                        <p class="text-center mb-2">{counter.renderState}...</p>
                                        <div class="w-full bg-gray-300 rounded-full dark:bg-gray-700">
                                            <div class="bg-blue-600 text-xs font-semibold text-blue-100 text-center p-0.5 leading-none rounded-l-full transition ease-in-out duration-150 formgear-progress-bar" classList={{
                                                'rounded-full': counter.renderProgress > 95 || counter.renderProgress < 5
                                            }} style={{ width: `${counter.renderProgress}%` }}> {counter.renderProgress}%</div>
                                        </div>
                                    </Show>
                                </div>
                                <div class="lg:col-span-2"></div>
                            </div>
                        </div>

                        {/* <div
                            class="bg-gray-50 dark:bg-gray-900 w-72 flex-shrink-0  dark:border-gray-800 h-full  p-5 space-y-4
            absolute inset-y-0 left-0 transform -translate-x-full transition-transform duration-500 ease-in-out md:relative md:translate-x-0 z-10">
                            <div class="animate-pulse flex space-x-4">
                                <div class="flex-1 space-y-3 py-1">
                                    <div class="w-full  shadow-2xl rounded-lg">

                                        <div class="h-32 bg-gray-200 rounded-tr-lg rounded-tl-lg animate-pulse"></div>

                                        <div class="p-5">
                                            <div class="h-20 px-2 rounded-lg bg-gray-200 animate-pulse mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                            <div class="h-20 px-2 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-grow bg-white dark:bg-gray-900 transition duration-500 ease-in-out z-10 p-5 space-y-4">
                            <div class="flex-grow bg-white dark:bg-gray-900 ">
                                <div class=" w-full mx-auto">
                                    <div class="animate-pulse flex space-x-4">
                                        <div class="flex-1 space-y-3 py-1">
                                            <div
                                                class="min-h-screen flex items-start justify-start bg-gradient-to-br from-gray-200 to-gray-400">

                                                <div class="w-full bg-white  shadow-2xl rounded-lg">

                                                    <div class="h-32 bg-gray-200 rounded-tr-lg rounded-tl-lg animate-pulse"></div>

                                                    <div class="p-5">
                                                        <div class="h-20 px-2 rounded-lg bg-gray-200 animate-pulse mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> {/* <div
                            class="bg-gray-50 dark:bg-gray-900 w-72 flex-shrink-0  dark:border-gray-800 h-full  p-5 space-y-4
            absolute inset-y-0 left-0 transform -translate-x-full transition-transform duration-500 ease-in-out md:relative md:translate-x-0 z-10">
                            <div class="animate-pulse flex space-x-4">
                                <div class="flex-1 space-y-3 py-1">
                                    <div class="w-full  shadow-2xl rounded-lg">

                                        <div class="h-32 bg-gray-200 rounded-tr-lg rounded-tl-lg animate-pulse"></div>

                                        <div class="p-5">
                                            <div class="h-20 px-2 rounded-lg bg-gray-200 animate-pulse mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                            <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                            <div class="h-20 px-2 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="flex-grow bg-white dark:bg-gray-900 transition duration-500 ease-in-out z-10 p-5 space-y-4">
                            <div class="flex-grow bg-white dark:bg-gray-900 ">
                                <div class=" w-full mx-auto">
                                    <div class="animate-pulse flex space-x-4">
                                        <div class="flex-1 space-y-3 py-1">
                                            <div
                                                class="min-h-screen flex items-start justify-start bg-gradient-to-br from-gray-200 to-gray-400">

                                                <div class="w-full bg-white  shadow-2xl rounded-lg">

                                                    <div class="h-32 bg-gray-200 rounded-tr-lg rounded-tl-lg animate-pulse"></div>

                                                    <div class="p-5">
                                                        <div class="h-20 px-2 rounded-lg bg-gray-200 animate-pulse mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>

                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-10 mb-4"></div>
                                                        <div class="h-6 rounded-lg bg-gray-200 animate-pulse mt-4 mb-4"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    )
}