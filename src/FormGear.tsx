import { render } from "solid-js/web";

import Form from "./Form";
import { FormProvider } from "./FormProvider";
import "./index.css";

import FormLoaderProvider from "./loader/FormLoaderProvider";
import Loader from "./loader/Loader";

import { setMedia } from "./stores/MediaStore";
import { note, setNote } from './stores/NoteStore';
import { preset, setPreset } from './stores/PresetStore';
import { remark, setRemark } from './stores/RemarkStore';
import { response, setResponse } from './stores/ResponseStore';
import { setTemplate, template } from './stores/TemplateStore';
import { setValidation, validation } from './stores/ValidationStore';
import { setCounter, counter } from './stores/CounterStore';

import { nested } from './stores/NestedStore';
import { reference, setReference } from './stores/ReferenceStore';
import { setSidebar } from './stores/SidebarStore';

import { createSignal } from "solid-js";

import semverCompare from "semver-compare";
import { toastInfo } from "./FormInput";

import mediaJSON from './data/default/media.json';
import presetJSON from './data/default/preset.json';
import referenceJSON from './data/default/reference.json';
import remarkJSON from './data/default/remark.json';
import responseJSON from './data/default/response.json';

import { checkPAPIMode, initReferenceMap, setEnableFalse } from "./GlobalFunction";
import { setConfigStore } from "./stores/ConfigStore";
import FormLoaderMain from "./loader/FormLoaderMain";
import packageJson from "../package.json"
import { ControlType } from "./FormType";

export const gearVersion = packageJson.version;
export let templateVersion = '0.0.0';
export let validationVersion = '0.0.0';
export function FormGear(referenceFetch, templateFetch, presetFetch, responseFetch, validationFetch, mediaFetch, remarkFetch, config, uploadHandler, GpsHandler, offlineSearch, mobileExit, setResponseMobile, setSubmitMobile, openMap) {

  render(() => (
    <FormLoaderMain />
  ), document.getElementById("FormGear-loader") as HTMLElement);

  console.log("   ______               _____            ");
  console.log("  /  /__/__ _____ _   / ___/__ ___ _____");
  console.log(" /  _// _ \\/ __/  ' \\/ (_ / -_) _ `/ __/");
  console.log("/__/  \\___/_/ /_/_/_/\\___/\\__/\\_,_/_/   %c@" + gearVersion, ' font-family:system-ui; font-weight: bold; color: #14b8a6;');

  let timeStart = new Date();
  let stuff = {
    "reference": referenceFetch,
    "template": templateFetch,
    "preset": presetFetch,
    "response": responseFetch,
    "validation": validationFetch,
    "media": mediaFetch,
    "remark": remarkFetch
  };

  let checkJson = (json: string, message: string) => {
    if (Object.keys(json).length == 0) {
      toastInfo(message, 5000, "", "bg-pink-600/80");
    }
  }

  Object.keys(stuff).map((key) => {
    checkJson(stuff[key], `Failed to fetch ${key} file`)
  })

  try {
    setTemplate({ details: templateFetch });
    setValidation({ details: validationFetch });
    setConfigStore(config);

    (Object.keys(presetFetch).length > 0) ? setPreset({ details: presetFetch }) : setPreset({ details: JSON.parse(JSON.stringify(presetJSON)) });
    (Object.keys(responseFetch).length > 0) ? setResponse({ details: responseFetch }) : setResponse({ details: JSON.parse(JSON.stringify(responseJSON)) });
    (Object.keys(mediaFetch).length > 0) ? setMedia({ details: mediaFetch }) : setMedia({ details: JSON.parse(JSON.stringify(mediaJSON)) });
    (Object.keys(remarkFetch).length > 0) ? setRemark({ details: remarkFetch }) : setRemark({ details: JSON.parse(JSON.stringify(remarkJSON)) });
    (Object.keys(responseFetch).length > 0 && response.details.counter !== undefined) && setCounter(JSON.parse(JSON.stringify(response.details.counter[0])))

    const tmpVarComp = []
    const tmpEnableComp = [];
    const flagArr = [];
    const refList = [];
    const sideList = [];
    const sidebarList = [];
    const referenceList = [];
    const nestedList = [];
    let len = template.details.components[0].length;
    let counterRender = counter.render;

    templateVersion = template.details.version ?? '0.0.1';
    validationVersion = validation.details.version ?? '0.0.1';

    const gearVersionState = template.details.version == undefined ? 1 :
      semverCompare(
        gearVersion,
        response.details.gearVersion ?? '0.0.0'
      );

    const templateVersionState = template.details.version == undefined ? 1 :
      semverCompare(
        templateVersion,
        response.details.templateVersion ?? '0.0.0'
      );

    const validationVersionState = validation.details.version == undefined ? 1 : semverCompare(
      validationVersion,
      response.details.validationVersion ?? '0.0.0'
    );

    (Object.keys(referenceFetch).length > 0) ? setReference(referenceFetch) : setReference(JSON.parse(JSON.stringify(referenceJSON)));
    const sidebarLen = reference.sidebar.length;
    const referenceLen = reference.details.length;

    let runAll = 0;
    if (gearVersionState == 0 && templateVersionState == 0 && validationVersionState == 0 && referenceLen > 0 && sidebarLen > 0) {
      console.log('Reuse reference ♻️');
      setReference(referenceFetch)
      initReferenceMap()
      setSidebar('details', referenceFetch.sidebar)
      runAll = 1;
      setCounter('render', counterRender += 1)
      render(() => (
        <FormProvider>
          <FormLoaderProvider>
            <Form config={config} timeStart={timeStart} runAll={runAll} tmpEnableComp={tmpEnableComp} tmpVarComp={tmpVarComp} template={template} preset={preset} response={response} validation={validation} remark={remark} uploadHandler={uploadHandler} GpsHandler={GpsHandler} offlineSearch={offlineSearch} mobileExit={mobileExit} setResponseMobile={setResponseMobile} setSubmitMobile={setSubmitMobile} openMap={openMap} />
            <Loader />
          </FormLoaderProvider>
        </FormProvider>
      ), document.getElementById("FormGear-root") as HTMLElement);
    } else {
      console.log('Build reference 🚀')
      let dataKeyCollections = [];

      const getValue = (dataKey: string) => {
        let answer = '';
        if (referenceList.length > 0) {
          const componentIndex = referenceList.findIndex(obj => obj.dataKey === dataKey);
          if (componentIndex !== -1 && (referenceList[componentIndex].answer) && (referenceList[componentIndex].enable)) answer = referenceList[componentIndex].answer;
        }
        return answer;
      }

      const [components, setComponents] = createSignal([]);

      const buildReference = (element, index) => {
        for (let j = 0; j < element.length; j++) {
          refList[j] = [];
          sideList[j] = [];
          flagArr[j] = 0;
          setTimeout(() => {
            try {
              const loopTemplate = (element, index, parent, level, sideEnable) => {
                let el_len = element.length
                for (let i = 0; i < el_len; i++) {
                  if (element[i].type != ControlType.Section && element[i].type != ControlType.InnerHTML) {
                    if (dataKeyCollections.includes(element[i].dataKey)) {
                      throw new Error('Duplicate dataKey on ' + element[i].dataKey);
                    }
                    dataKeyCollections.push(element[i].dataKey)
                  }
                  let answer = element[i].answer;

                  let el_type = element[i].type
                  if ((el_type == ControlType.ListTextInputRepeat || el_type == ControlType.ListSelectInputRepeat)) {
                    answer = JSON.parse(JSON.stringify(answer));
                  } else if (el_type == ControlType.VariableInput && level < 2) {
                    (answer == undefined) && (!sideEnable) && tmpVarComp.push(JSON.parse(JSON.stringify(element[i])));
                  }

                  let components
                  if (el_type == ControlType.NestedInput) {
                    let nestPosition = nested.details.findIndex(obj => obj.dataKey === element[i].dataKey);
                    components = (nestPosition !== -1) ? nested.details[nestPosition].components : (element[i].components) ? element[i].components : undefined;
                  } else {
                    components = (element[i].components) ? element[i].components : undefined;
                  }

                  if (el_type == ControlType.Section || (el_type == ControlType.NestedInput && components.length > 1)) {
                    if (element[i].enableCondition !== undefined) {
                      tmpEnableComp.push(JSON.parse(JSON.stringify(element[i])));
                      sideEnable = false;
                    } else {
                      sideEnable = true;
                    }

                    let sideListLen = sideList[j].length;
                    sideList[j][sideListLen] = {
                      dataKey: element[i].dataKey,
                      name: element[i].name,
                      label: element[i].label,
                      description: element[i].description,
                      level: level,
                      index: parent.concat(i),
                      components: components,
                      sourceQuestion: element[i].sourceQuestion ?? '',
                      enable: sideEnable,
                      enableCondition: element[i].enableCondition ?? '',
                      componentEnable: element[i].componentEnable ?? []
                    }
                    sideList[j][sideListLen] = checkPAPIMode(sideList[j][sideListLen])
                  }
                  if (el_type == ControlType.NestedInput) {
                    let nestedLen = nestedList.length
                    nestedList[nestedLen] = new Array(
                      {
                        dataKey: element[i].dataKey,
                        name: element[i].name,
                        label: element[i].label,
                        description: element[i].description,
                        level: level,
                        index: parent.concat(i),
                        components: components,
                        sourceQuestion: element[i].sourceQuestion ?? '',
                        enable: sideEnable,
                        enableCondition: element[i].enableCondition ?? '',
                        componentEnable: element[i].componentEnable ?? []
                      }
                    )
                    nestedList[nestedLen][0] = checkPAPIMode(nestedList[nestedLen][0])
                  }

                  if (el_type > ControlType.NestedInput && element[i].enableCondition !== undefined && !sideEnable) tmpEnableComp.push(JSON.parse(JSON.stringify(element[i])));

                  let vals;
                  let compVal;
                  let valPosition = validation.details.testFunctions.findIndex(obj => obj.dataKey === element[i].dataKey);
                  if (valPosition !== -1) {
                    vals = validation.details.testFunctions[valPosition].validations;
                    compVal = validation.details.testFunctions[valPosition].componentValidation;
                  }

                  let hasRemark = false;
                  if (element[i].enableRemark === undefined || (element[i].enableRemark !== undefined && element[i].enableRemark)) {
                    let remarkPosition = remark.details.notes.findIndex(obj => obj.dataKey === element[i].dataKey);
                    if (remarkPosition !== -1) {
                      let newNote = remark.details.notes[remarkPosition];
                      let updatedNote = JSON.parse(JSON.stringify(note.details.notes));
                      updatedNote.push(newNote);
                      hasRemark = true;
                      setNote('details', 'notes', updatedNote);
                    }
                  }

                  let refListLen = refList[j].length;
                  refList[j][refListLen] = {
                    dataKey: element[i].dataKey,
                    name: element[i].name,
                    label: element[i].label,
                    hint: element[i].hint ?? '',
                    description: element[i].description,
                    type: element[i].type,
                    answer: answer,
                    index: parent.concat(i),
                    level: level,
                    options: element[i].options,
                    sourceQuestion: element[i].sourceQuestion,
                    urlValidation: element[i].urlValidation,
                    currency: element[i].currency,
                    source: element[i].source,
                    urlPath: element[i].path,
                    parent: element[i].parent,
                    separatorFormat: element[i].separatorFormat,
                    isDecimal: element[i].isDecimal,
                    maskingFormat: element[i].maskingFormat,
                    expression: element[i].expression,
                    componentVar: element[i].componentVar,
                    render: element[i].render,
                    renderType: element[i].renderType,
                    enable: true,
                    enableCondition: element[i].enableCondition ?? '',
                    componentEnable: element[i].componentEnable ?? [],
                    enableRemark: element[i].enableRemark ?? true,
                    client: element[i].client,
                    titleModalDelete: element[i].titleModalDelete,
                    sourceOption: element[i].sourceOption,
                    sourceSelect: element[i].sourceSelect,
                    typeOption: element[i].typeOption,
                    contentModalDelete: element[i].contentModalDelete,
                    validationState: element[i].validationState ?? 0,
                    validationMessage: element[i].validationMessage ?? [],
                    validations: vals,
                    componentValidation: compVal,
                    hasRemark: hasRemark,
                    rows: (element[i].rows !== undefined && element[i].rows[0] !== undefined) ? element[i].rows : undefined,
                    cols: (element[i].cols !== undefined && element[i].cols[0] !== undefined) ? element[i].cols : undefined,
                    rangeInput: (element[i].rangeInput !== undefined && element[i].rangeInput[0] !== undefined) ? element[i].rangeInput : undefined,
                    lengthInput: (element[i].lengthInput !== undefined && element[i].lengthInput[0] !== undefined) ? element[i].lengthInput : undefined,
                    principal: element[i].principal,
                    columnName: element[i].columnName ?? '',
                    titleModalConfirmation: element[i].titleModalConfirmation,
                    contentModalConfirmation: element[i].contentModalConfirmation,
                    required: element[i].required,
                    presetMaster: element[i].presetMaster,
                    disableInput: element[i].disableInput,
                    decimalLength: element[i].decimalLength,
                    disableInitial: element[i].disableInitial,
                    nestedType: element[i].nestedType,
                  }
                  refList[j][refListLen] = checkPAPIMode(refList[j][refListLen])

                  element[i].components && element[i].components.forEach((element) => loopTemplate(element, refListLen, parent.concat(i, 0), level + 1, sideEnable))
                }
              }

              let hasSideEnable = false;
              if (element[j].enableCondition !== undefined) {
                tmpEnableComp.push(JSON.parse(JSON.stringify(element[j])));
                hasSideEnable = true;
              }

              sideList[j][0] = {
                dataKey: element[j].dataKey,
                name: element[j].name,
                label: element[j].label,
                description: element[j].description,
                level: 0,
                index: [0, j],
                components: element[j].components,
                sourceQuestion: element[j].sourceQuestion ?? '',
                enable: !hasSideEnable,
                enableCondition: element[j].enableCondition ?? '',
                componentEnable: element[j].componentEnable ?? []
              }
              sideList[j][0] = checkPAPIMode(sideList[j][0])

              // insert section
              refList[j][0] = {
                dataKey: element[j].dataKey,
                name: element[j].name,
                label: element[j].label,
                hint: (element[j].hint) ? element[j].hint : '',
                description: element[j].description,
                type: element[j].type,
                index: [0, j],
                level: 0,
                options: element[j].options,
                sourceQuestion: element[j].sourceQuestion,
                urlValidation: element[j].urlValidation,
                currency: element[j].currency,
                source: element[j].source,
                urlPath: element[j].path,
                parent: element[j].parent,
                separatorFormat: element[j].separatorFormat,
                isDecimal: element[j].isDecimal,
                typeOption: element[j].typeOption,
                sourceOption: element[j].sourceOption,
                maskingFormat: element[j].maskingFormat,
                expression: element[j].expression,
                componentVar: element[j].componentVar,
                render: element[j].render,
                renderType: element[j].renderType,
                enable: true,
                enableCondition: element[j].enableCondition ?? '',
                componentEnable: element[j].componentEnable ?? [],
                enableRemark: element[j].enableRemark ?? true,
                client: element[j].client,
                titleModalDelete: element[j].titleModalDelete,
                contentModalDelete: element[j].contentModalDelete,
                validationState: element[j].validationState ?? 0,
                validationMessage: element[j].validationMessage ?? [],
                rows: (element[j].rows !== undefined && element[j].rows[0] !== undefined) ? element[j].rows : undefined,
                cols: (element[j].cols !== undefined && element[j].cols[0] !== undefined) ? element[j].cols : undefined,
                rangeInput: (element[j].rangeInput !== undefined && element[j].rangeInput[0] !== undefined) ? element[j].rangeInput : undefined,
                lengthInput: (element[j].lengthInput !== undefined && element[j].lengthInput[0] !== undefined) ? element[j].lengthInput : undefined,
                principal: element[j].principal,
                columnName: element[j].columnName ?? '',
                titleModalConfirmation: element[j].titleModalConfirmation,
                contentModalConfirmation: element[j].contentModalConfirmation,
                required: element[j].required,
              }
              refList[j][0] = checkPAPIMode(refList[j][0])

              loopTemplate(element[j].components[0], 0, [0, j, 0], 1, hasSideEnable)

              flagArr[j] = 1;
            } catch (error) {
              toastInfo(error.message, 5000, "", "bg-pink-600/80");
            }
          },
            500)
        }
      }
      template.details.components.forEach((element, index) => buildReference(element, index))
      runAll = 0;

      let sum = 0;
      const t = setInterval(() => {
        sum = 0;
        for (let a = 0; a < len; a++) {
          if (flagArr[a] == 1) sum++;
        }

        if (sum === len) {
          clearInterval(t)
          for (let x = 0; x < sideList.length; x++) {
            sidebarList.push(sideList[x][0])
          }
          for (let j = 0; j < refList.length; j++) {
            for (let k = 0; k < refList[j].length; k++) {
              referenceList.push(refList[j][k])
            }
          }
          let arrIndex = []
          let newData = new Object()
          //mulai di loop
          function loopnested(dataKey, len, level) {
            let nestedPos = referenceList.findIndex(obj => obj.dataKey == dataKey)
            let newSetComp = []
            let counter = 0
            for (let x = Number(nestedPos) + 1; x <= referenceList.length; x++) {
              if (level == referenceList[x].level - 1) {
                if (referenceList[x].type > 2 && !arrIndex.includes(Number(x))) {
                  arrIndex.push(Number(x))
                  newData[Number(x)] = referenceList[x].dataKey
                }
                newSetComp.push(referenceList[x])
                counter++
              }
              if (counter == len) break;
            }

            referenceList[nestedPos].components = [JSON.parse(JSON.stringify(newSetComp))]
          }
          for (let z = (nestedList.length - 1); z >= 0; z--) {
            loopnested(nestedList[z][0].dataKey, Number(nestedList[z][0].components[0].length), nestedList[z][0].level)
          }

          let newIn = Object.keys(newData)
          let arrIndexLen = Object.keys(newData).length
          for (let counter = arrIndexLen - 1; counter >= 0; counter--) {
            referenceList.splice(Number(newIn[counter]), 1)
          }

          initReferenceMap(referenceList)
          setReference('details', referenceList)
          setSidebar('details', sidebarList)
          setEnableFalse()
          setCounter('render', counterRender += 1)
          render(() => (
            <FormProvider>
              <FormLoaderProvider>
                <Form config={config} timeStart={timeStart} runAll={runAll} tmpEnableComp={tmpEnableComp} tmpVarComp={tmpVarComp} template={template} preset={preset} response={response} validation={validation} remark={remark} uploadHandler={uploadHandler} GpsHandler={GpsHandler} offlineSearch={offlineSearch} mobileExit={mobileExit} setResponseMobile={setResponseMobile} setSubmitMobile={setSubmitMobile} openMap={openMap} />
                <Loader />
              </FormLoaderProvider>
            </FormProvider>
          ), document.getElementById("FormGear-root") as HTMLElement);
        }
      }, 500)
    }
  } catch (e: unknown) {
    console.error(e)
    toastInfo("Failed to render the questionnaire", 5000, "", "bg-pink-600/80");
  };

}
