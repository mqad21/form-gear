import { For, Switch, Match, Show, createSignal, createEffect } from "solid-js";
import { FormComponentBase } from "../../FormType";
import { InputContainer } from "./partials";

const VariableInput: FormComponentBase = props => {

  const [answer, setAnswer] = createSignal(props.value)

  createEffect(() => {
    setAnswer(props.value);
  })

  return (
    <Show when={(props.component.render)}>
      <InputContainer {...props}>
        <Switch>
          <Match when={(props.component.render) && props.component.renderType <= 1}>
            <input value={props.value}
              disabled
            />
            <small>{props.validationMessage}</small>
          </Match>
          <Match when={(props.component.render) && props.component.renderType === 2}>
            <div class="grid space-y-4">
              <For each={props.value}>
                {(item: any, index) => (
                  <input 
                  class="formgear-input-papi"
                  value={item.label}
                    disabled
                  />
                )}
              </For>
            </div>
          </Match>
        </Switch>
      </InputContainer>
    </Show>
  )
}

export default VariableInput