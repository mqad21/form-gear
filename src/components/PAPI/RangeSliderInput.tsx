import { createSignal, For, Match, Show, Switch } from "solid-js"
import { FormComponentBase } from "../../FormType";
import { InputContainer } from "./partials";

const RangeSliderInput: FormComponentBase = props => {
  return (
    <InputContainer {...props}>
      <input value={props.value || 0}
        type="number"
        min={props.component.rangeInput[0].min}
        max={props.component.rangeInput[0].max}
        step={props.component.rangeInput[0].step}
        onChange={(e) => props.onValueChange(e.currentTarget.value)} />
    </InputContainer>
  )
}

export default RangeSliderInput