import { createSignal, createEffect, For, Show, Switch, Match } from "solid-js"
import { FormComponentBase } from "../../FormType"
import { createInputMask } from "@solid-primitives/input-mask"
import { InputContainer } from "./partials"

const MaskingInput: FormComponentBase = props => {

  let ref
  const formatMask = createInputMask(props.component.maskingFormat);

  const inputMask = {
    ref,
    get value() {
      return inputMask.ref?.value;
    }
  };

  let handleOnChange = (value: any) => {
    props.onValueChange(value)
  }

  createEffect(() => {
    document.getElementById("inputMask" + props.component.dataKey).click()

  })

  return (
    <InputContainer {...props}>
      <input value={props.value}
        id={"inputMask" + props.component.dataKey}
        ref={inputMask.ref}
        placeholder={props.component.maskingFormat.replace(/[a]/g, '__').replace(/[9]/g, '#')}
        onChange={(e) => handleOnChange(e.currentTarget.value)}
        onclick={formatMask}
        oninput={formatMask}
        onpaste={formatMask}
      />
    </InputContainer>
  )
}

// transition ease-in-out m-0
// focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"

export default MaskingInput