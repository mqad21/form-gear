import { FormComponentBase } from "../../FormType";
import { InputContainer } from "./partials";


const ToggleInput: FormComponentBase = props => {

    const settedValue = props.value ? 1 : 0

    const handleChange = (e) => {
        const value = e.target.value == 1
        props.onValueChange(value)
    }


    return (
        <InputContainer {...props}>
            <input onChange={handleChange} type="number" value={settedValue} />
        </InputContainer>
    )
}

export default ToggleInput