import { createSignal, Show, For, Switch, Match, createMemo } from "solid-js"
import { handleInputFocus, handleInputKeyDown } from "../../events"
import { FormComponentBase } from "../../FormType"
import { reference } from "../../stores/ReferenceStore"
import { InputContainer } from "./partials"

const NumberInput: FormComponentBase = props => {

  const isUsedInNested = createMemo(() => {
    return reference.details.findIndex((ref: any) => ref.sourceQuestion === props.component.dataKey) !== -1
  })

  return (
    <>
      <Show when={props.component.lengthInput === undefined}>
        <InputContainer {...props}>
          <input
            value={props.value}
            type="number"
            placeholder=""
            onInput={(e) => {
              if (isUsedInNested())
                props.onValueChange(e.currentTarget.value);
            }}
            onChange={(e) => {
              if (!isUsedInNested())
                props.onValueChange(e.currentTarget.value);
            }}
          />
        </InputContainer>
      </Show>
      <Show when={props.component.lengthInput !== undefined && props.component.lengthInput.length > 0}>
        <InputContainer {...props}>
          <input value={props.value}
            type="number"
            placeholder=""
            onInput={(e) => {
              props.onValueChange(e.currentTarget.value);
            }}
            onChange={(e) => {
              if (!isUsedInNested())
                props.onValueChange(e.currentTarget.value);
            }}
            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
            maxlength={props.component.lengthInput[0].maxlength !== undefined ? props.component.lengthInput[0].maxlength : ''}
            minlength={props.component.lengthInput[0].minlength !== undefined ? props.component.lengthInput[0].minlength : ''}
            max={props.component.rangeInput ? props.component.rangeInput[0].max !== undefined ? props.component.rangeInput[0].max : '' : ''}
            min={props.component.rangeInput ? props.component.rangeInput[0].min !== undefined ? props.component.rangeInput[0].min : '' : ''}
          />
        </InputContainer>
      </Show>
    </>
  )
}

export default NumberInput