import { createSignal, For, Match, Show, Switch } from "solid-js"
import { FormComponentBase } from "../../FormType"
import { InputContainer } from "./partials"

const TextAreaInput: FormComponentBase = props => {

  return (
    <>
      <Show when={props.component.lengthInput === undefined}>
        <InputContainer {...props}>
          <textarea
            value={props.value}
            rows={props.component.rows || 2}
            onChange={(e) => {
              props.onValueChange(e.currentTarget.value)
            }}
          />
        </InputContainer>
      </Show>
      <Show when={props.component.lengthInput !== undefined && props.component.lengthInput.length > 0}>
        <InputContainer {...props}>

          <textarea value={props.value}
            rows={props.component.rows || 2}
            onChange={(e) => {
              props.onValueChange(e.currentTarget.value)
            }}
            maxlength={props.component.lengthInput[0].maxlength !== undefined ? props.component.lengthInput[0].maxlength : ''}
            minlength={props.component.lengthInput[0].minlength !== undefined ? props.component.lengthInput[0].minlength : ''}
          />
        </InputContainer>
      </Show>
    </>
  )
}

export default TextAreaInput