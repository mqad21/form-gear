import { FormComponentBase } from "../../FormType"
import { InputContainer } from "./partials"

const SignatureInput: FormComponentBase = props => {

  const settedValue = props.value?.[0]?.value ? 1 : 0

  const handleChange = (e) => {
    const value = e.target.value == 1
    const updatedAnswer = [{ value, type: undefined, signature: undefined }]
    props.onValueChange(updatedAnswer)
  }

  return (
    <InputContainer {...props}>
      <input onChange={handleChange} type="number" value={settedValue} />
    </InputContainer>
  )
}

export default SignatureInput