import { ControlType, FormComponentBase, NOT_INPUT_CONTROL, REPEATABLE_INPUT_CONTROL } from "../../FormType"
import { For, createMemo, Switch, Match, Show, createSignal, createRenderEffect, createEffect, onMount, createResource } from 'solid-js'
import { reference, setReference } from '../../stores/ReferenceStore';
import { locale } from "../../stores/LocaleStore";
import Toastify from 'toastify-js'
import { sidebar } from "../../stores/SidebarStore";
import { input, setInput } from '../../stores/InputStore';
import { cleanLabel, getContainer, goToTab } from "../../GlobalFunction";
import { handleInputFocus, handleInputKeyDown } from "../../events";
import FormInput from "../../FormInput";
import { createOptions, Select } from "@thisbeyond/solid-select";
import { Option } from "@thisbeyond/solid-select/dist/types/create-select";
import { NestedType } from "../../constants";

const NestedInput: FormComponentBase = props => {
	const config = props.config
	const [flag, setFlag] = createSignal(0);
	const [edited, setEdited] = createSignal(0);
	const [tmpInput, setTmpInput] = createSignal<any>();
	const [disableInput] = createSignal((config.formMode > 1) ? true : props.component.disableInput)
	const [instruction, setInstruction] = createSignal('');
	const [hasNestedComponent, setHasNestedComponent] = createSignal(true);
	const [isAppendable, setIsAppendable] = createSignal(true);
	const [hoveredTabIndex, setHoveredTabIndex] = createSignal(null);
	const [sourceQuestionComponent, setSourceQuestionComponent] = createSignal<any>();

	let newInputRef, editInputRef, tabRef, options, getOptions;

	const dataSticky = 66 + getContainer()?.getBoundingClientRect().top

	onMount(() => {
		if (!input.nestedTabOffsetTop?.[props.component.dataKey]) {
			const clientRectTop = tabRef?.offsetTop - getContainer()?.offsetTop
			setInput("nestedTabOffsetTop", { [props.component.dataKey]: clientRectTop })
		}
	})

	createRenderEffect(() => {
		if (props.component.nestedType !== undefined) {
			setHasNestedComponent(props.component.nestedType === NestedType.COLUMN)
		} else {
			const nestedIndex = props.component.components[0].findIndex((item: any) => {
				return item.type == ControlType.NestedInput
			})
			setHasNestedComponent(nestedIndex !== -1)
		}

		const sourceQuestionComponent = reference.details.find(element => element.dataKey == props.component.sourceQuestion)
		setSourceQuestionComponent(sourceQuestionComponent)
		setIsAppendable(REPEATABLE_INPUT_CONTROL.includes(sourceQuestionComponent?.type))
	})

	let componentAnswerIndex = createMemo(() => {
		return String(reference.details.findIndex(obj => obj.dataKey == props.component.sourceQuestion));
	})

	let localAnswer = createMemo(() => {
		const componentAnswerIndex = reference.details.findIndex(obj => obj.dataKey == props.component.sourceQuestion);
		return reference.details[componentAnswerIndex].answer
	})

	let sourceAnswer = createMemo(() => {
		let answer = [];
		if (props.component.sourceQuestion !== '') {
			const componentAnswerIndex = reference.details.findIndex(obj => obj.dataKey == props.component.sourceQuestion);
			if (reference.details[componentAnswerIndex]) {
				if (typeof reference.details[componentAnswerIndex].answer === 'object') {
					answer = reference.details[componentAnswerIndex].answer == '' ? [] : reference.details[componentAnswerIndex].answer;
					if (reference.details[componentAnswerIndex].type == 21 || reference.details[componentAnswerIndex].type == 22) {
						let tmpAnswer = JSON.parse(JSON.stringify(answer));
						tmpAnswer.splice(0, 1);
						answer = tmpAnswer;
					}
				} else {
					answer = reference.details[componentAnswerIndex].answer == '' ? 0 : reference.details[componentAnswerIndex].answer;
					let dummyArrayAnswer = [];
					for (let i = 1; i <= Number(answer); i++) {
						dummyArrayAnswer.push({ value: i, label: i });
					}
					answer = dummyArrayAnswer;
				}
			}
		}
		return answer;
	})

	let validationMessages = createMemo(() => {
		const messages = []
		const dataKeys = []
		sourceAnswer()?.forEach((item: any) => {
			const key = props.component.dataKey + '#' + item.value
			const position = sidebar.details.findIndex(obj => obj.dataKey == key);
			const components = sidebar.details[position]?.components[0]
			components?.forEach((component: any) => {
				if (component.dataKey == input.currentDataKey) {
					dataKeys.push(component.dataKey)
				}
			})
		})

		reference.details?.forEach((detail: any) => {
			if (dataKeys.includes(detail.dataKey)) {
				messages.push({
					message: detail.validationMessage,
					state: detail.validationState
				})
			}
		})

		return messages
	})

	let getLastId = createMemo(() => {
		const lastAnswer = sourceAnswer()[sourceAnswer().length - 1]
		if (lastAnswer == undefined) return 0
		return Number(lastAnswer?.value);
	})

	const toastInfo = (text: string) => {
		Toastify({
			text: (text == '') ? locale.details.language[0].componentDeleted : text,
			duration: 3000,
			gravity: "top",
			position: "right",
			stopOnFocus: true,
			className: "bg-blue-600/80",
			style: {
				background: "rgba(8, 145, 178, 0.7)",
				width: "400px"
			}
		}).showToast();
	}

	const modalDelete = () => {
		let titleModal = document.querySelector("#titleModalDelete");
		let contentModal = document.querySelector("#contentModalDelete");
		titleModal.innerHTML = props.component.titleModalDelete !== undefined ? props.component.titleModalDelete : 'Confirm Delete?';
		contentModal.innerHTML = props.component.contentModalDelete !== undefined ? props.component.contentModalDelete : 'Deletion will also delete related components, including child components from this parent.';
	}

	const showInstruction = (dataKey) => {
		(instruction() == dataKey) ? setInstruction("") : setInstruction(dataKey);
	}

	let handleOnPlus = () => {
		if (flag() === 0 && edited() === 0) {
			setFlag(1);
			setEdited(0);
			newInputRef?.focus()
		} else {
			toastInfo(locale.details.language[0].componentNotAllowed);
		}
	}

	let handleOnSave = (id: number, e, localAnswer, isEdit: boolean = false) => {
		const value = e.target.value.trim()
		switch (sourceQuestionComponent()?.type) {
			case ControlType.ListSelectInputRepeat:
				if (value !== "") {
					setTmpInput(getOptions().find(it => it.value == value))
				}
				if (tmpInput() && tmpInput()?.value !== 0) {
					let updatedAnswer = JSON.parse(JSON.stringify(localAnswer));
					if (!isEdit) {//insert
						if (updatedAnswer.length == 0) {
							updatedAnswer = [...updatedAnswer, {
								"label": "lastId#0",
								"value": "0"
							}];
						}
						updatedAnswer = [...updatedAnswer, tmpInput()];
					} else {//update
						let answerIndex = updatedAnswer.findIndex((item) => item.value == id)
						updatedAnswer.splice(answerIndex, 1, tmpInput());
					}

					props.onValueChange(updatedAnswer, props.component.sourceQuestion);

					if (!isEdit) {
						toastInfo(locale.details.language[0].componentAdded);
					} else {
						toastInfo(locale.details.language[0].componentEdited);
					}
				} else {
					if (!isEdit) {
						toastInfo(locale.details.language[0].componentEmpty);
					}
				}
				setFlag(0);
				setEdited(0);
				break;
			default:
				if (value !== "") {
					setTmpInput(value)
				}
				setTimeout(() => {
					if (tmpInput() !== "") {
						let updatedAnswer = JSON.parse(JSON.stringify(localAnswer));
						if (!isEdit) {
							updatedAnswer = [...updatedAnswer, { "value": id, "label": tmpInput() }];
							updatedAnswer[0].label = "lastId#" + id;
						} else {
							let answerIndex = updatedAnswer.findIndex((item) => item.value == id)
							updatedAnswer[answerIndex].label = tmpInput();
						}

						props.onValueChange(updatedAnswer, props.component.sourceQuestion);

						if (!isEdit) {
							toastInfo(locale.details.language[0].componentAdded);
						} else {
							toastInfo(locale.details.language[0].componentEdited);
						}
						setFlag(0);
						setEdited(0);
					} else {
						if (!isEdit) {
							toastInfo(locale.details.language[0].componentEmpty);
						} else {
							setFlag(0);
							setEdited(0);
						}
					}

				}, 500)
		}


	}

	let handleOnEdit = (id: number) => {
		if (flag() === 0 && edited() === 0) {
			setFlag(1);
			setEdited(id);
			editInputRef?.focus()
		} else {
			setEdited(id);
			editInputRef?.focus()
		}
	}

	let handleOnDelete = (id: number) => {
		if (flag() === 0 && edited() === 0) {
			setFlag(2);
			setEdited(id);
			modalDelete();
		} else if (flag() === 1) {
			toastInfo("Only 1 component is allowed to edit");
		} else if (flag() === 2) {
			let updatedAnswer = JSON.parse(JSON.stringify(localAnswer()));
			let answerIndex = updatedAnswer.findIndex((item) => item.value == id);
			updatedAnswer.splice(answerIndex, 1);

			props.onValueChange(updatedAnswer, props.component.sourceQuestion);
			toastInfo(locale.details.language[0].componentDeleted);
			setFlag(0);
			setEdited(0);
		}
	}

	let handleOnCancel = () => {
		setTmpInput('');
		setFlag(0);
		setEdited(0);
	}

	let handleTabClick = (index: number) => {
		setInput("currentTabIndex", { [props.component.dataKey]: index })
	}

	if (sourceQuestionComponent()?.type === ControlType.ListSelectInputRepeat) {
		switch (sourceQuestionComponent()?.typeOption) {
			case 1: {
				try {
					getOptions = createMemo(() => {
						let options = JSON.parse(JSON.stringify(sourceQuestionComponent()?.options));
						const localAnswerLength = localAnswer().length;
						let j = 0;
						if (localAnswer()[0] !== undefined) {
							j = (localAnswer()[0].value == 0) ? 1 : 0;
						}
						for (j; j < localAnswerLength; j++) {
							if (edited() === 0 || edited() !== Number(localAnswer()[j].value)) {
								let optionsIndex = options.findIndex((item) => item.value == localAnswer()[j].value);
								options.splice(optionsIndex, 1);
							}
						}
						return options
					})
				} catch (e) {
					// setisError(true)
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}
				break
			}
			case 2: {
				try {
					if (config.lookupMode === 1) {
						let url
						let params
						let urlHead
						let urlParams

						params = sourceQuestionComponent()?.sourceSelect
						// url = `${config.baseUrl}/${params[0].id}`
						url = `${config.baseUrl}/${params[0].id}/filter?version=${params[0].version}`
						if (params[0].parentCondition.length > 0) {
							urlHead = url

							urlParams = params[0].parentCondition.map((item, index) => {
								let newParams = item.value.split('@');
								let tobeLookup = reference.details.find(obj => obj.dataKey == newParams[0])
								if (tobeLookup.answer) {
									if (tobeLookup.answer.length > 0) {
										let parentValue = encodeURI(tobeLookup.answer[tobeLookup.answer.length - 1].value)
										url = `${config.lookupKey}=${item.key}&${config.lookupValue}=${parentValue}`
									}
								} else {
									url = `${config.lookupKey}=${item.key}&${config.lookupValue}=''`
								}
								return url
							}).join('&')
							// url = `${urlHead}?${urlParams}`
							url = `${urlHead}&${urlParams}`
						}
						const [fetched] = createResource<Option>(url, props.MobileOnlineSearch);
						let arr = []

						getOptions = createMemo(() => {
							if (fetched()) {
								if (!fetched().success) {
									// setisError(true)
									// toastInfo(locale.details.language[0].fetchFailed)
								} else {
									let cekValue = params[0].value
									let cekLabel = params[0].desc
									fetched().data.map((item, value) => {
										arr.push(
											{
												value: item[cekValue],
												label: item[cekLabel],
											}
										)
									})
									options = arr
									const localAnswerLength = localAnswer().length;
									let j = 0;
									if (localAnswer()[0] !== undefined) {
										j = (localAnswer()[0].value == 0) ? 1 : 0;
									}
									for (j; j < localAnswerLength; j++) {
										if (edited() === 0 || edited() !== Number(localAnswer()[j].value)) {
											let optionsIndex = options.findIndex((item) => item.value == localAnswer()[j].value);
											options.splice(optionsIndex, 1);
										}
									}
									return options
								}
							}
						})
					} else if (config.lookupMode === 2) {
						let params
						let tempArr = []
						params = sourceQuestionComponent()?.sourceSelect
						let id = params[0].id
						let version = params[0].version
						if (params[0].parentCondition.length > 0) {
							params[0].parentCondition.map((item, index) => {
								let newParams = item.value.split('@');
								let tobeLookup = reference.details.find(obj => obj.dataKey == newParams[0])
								if (tobeLookup.answer) {
									if (tobeLookup.answer.length > 0) {
										let parentValue = tobeLookup.answer[tobeLookup.answer.length - 1].value.toString()
										tempArr.push({ "key": item.key, "value": parentValue })
									}
								}
							})
						}
						let getResult = (result) => {
							getOptions = createMemo(() => {
								if (!result.success) {
									// setisError(true)
									// toastInfo(locale.details.language[0].fetchFailed)
								} else {
									let arr = []
									if (result.data.length > 0) {
										let cekValue = params[0].value
										let cekLabel = params[0].desc
										result.data.map((item, value) => {
											arr.push(
												{
													value: item[cekValue],
													label: item[cekLabel],
												}
											)
										})
										options = arr;
										const localAnswerLength = localAnswer().length;
										let j = 0;
										if (localAnswer()[0] !== undefined) {
											j = (localAnswer()[0].value == 0) ? 1 : 0;
										}
										for (j; j < localAnswerLength; j++) {
											if (edited() === 0 || edited() !== Number(localAnswer()[j].value)) {
												let optionsIndex = options.findIndex((item) => item.value == localAnswer()[j].value);
												options.splice(optionsIndex, 1);
											}
										}
										return options
									}
								}
							})

						}
						const fetched = props.MobileOfflineSearch(id, version, tempArr, getResult);
					}

				} catch (e) {
					// setisError(true)
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}
				break;
			}
			case 3: {
				try {
					getOptions = createMemo(() => {
						let options = sourceQuestionComponent()?.sourceOption !== undefined ? [] : JSON.parse(JSON.stringify(sourceQuestionComponent()?.options));
						if (sourceQuestionComponent()?.sourceOption !== undefined) {
							const componentAnswerIndex = reference.details.findIndex(obj => obj.dataKey === sourceQuestionComponent()?.sourceOption);
							if ((reference.details[componentAnswerIndex].type === 21 || 22 || 23 || 26 || 27 || 29)
								|| (reference.details[componentAnswerIndex].type === 4 && reference.details[componentAnswerIndex].renderType === 2)) {
								if (reference.details[componentAnswerIndex].answer) {
									options = JSON.parse(JSON.stringify(reference.details[componentAnswerIndex].answer))
								} else {
									options = []
								}
							}
						}
						const localAnswerLength = localAnswer().length;

						let j = 0;
						if (localAnswer()[0] !== undefined) {
							j = (localAnswer()[0].value == 0) ? 1 : 0;
						}

						for (j; j < localAnswerLength; j++) {
							if (edited() === 0 || edited() !== Number(localAnswer()[j].value)) {
								let optionsIndex = options.findIndex((item) => item.value == localAnswer()[j].value);
								options.splice(optionsIndex, 1);
							}
						}

						return options

					})
				} catch (e) {
					// setisError(true)
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}

				break

			}
			default: {
				try {
					getOptions = createMemo(() => {
						let options
						if (sourceQuestionComponent()?.options) {
							options = JSON.parse(JSON.stringify(sourceQuestionComponent()?.options));
							const localAnswerLength = localAnswer().length;
							let j = 0;
							if (localAnswer()[0] !== undefined) {
								j = (localAnswer()[0].value == 0) ? 1 : 0;
							}

							for (j; j < localAnswerLength; j++) {
								if (edited() === 0 || edited() !== Number(localAnswer()[j].value)) {
									let optionsIndex = options.findIndex((item) => item.value == localAnswer()[j].value);
									options.splice(optionsIndex, 1);
								}
							}
						} else {
							options = [];
						}
						return options

					})
				} catch (e) {
					// setisError(true)
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}

				break;
			}
		}
	}

	const AddInput = () => {
		return (<>
			<input
				name={props.component.dataKey + "#" + (getLastId() + 1)}
				onFocus={e => handleInputFocus(e, props)}
				onKeyDown={e => handleInputKeyDown(e, props)}
				onChange={e => handleOnSave((getLastId() + 1), e, localAnswer())} ref={newInputRef} type="text" class="w-full rounded font-semiboldpx-4 py-2.5 text-sm text-gray-700 bg-white bg-clip-padding transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400 formgear-nested-identifier" />
		</>)
	}

	const EditInput = ({ item, id, readOnly = false }) => {
		props.isNestedInput = true
		props.component.nestedType = NestedType.GRID
		return <input
			value={sourceQuestionComponent()?.type == ControlType.ListTextInputRepeat ? item.label : item.value}
			name={props.component.dataKey + "#" + id}
			onFocus={e => handleInputFocus(e, props)}
			onKeyDown={e => handleInputKeyDown(e, props)}
			readOnly={readOnly}
			onChange={e => handleOnSave(item.value, e, localAnswer(), true)} ref={editInputRef} type="text" class="formgear-input-papi" />
	}

	return (
		<div class="dark:border-gray-200/[.10] p-2"
			classList={{
				'border-b border-gray-300/[.40]': sourceAnswer().length > 0
			}}	>
			<Show when={props.component.required || props.component.hint}>
				<div class="font-semiboldtext-sm space-y-2 py-2.5 px-2 mb-4">
					<div class="inline-flex space-x-2">
						<Show when={props.component.required}>
							<span class="text-pink-600">*</span>
						</Show>
						<Show when={props.component.hint}>
							<button class="bg-transparent text-gray-300 rounded-full focus:outline-none h-4 w-4 hover:bg-gray-400 hover:text-white flex justify-center items-center"
								onClick={showInstruction}>
								<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
									<path stroke-linecap="round" stroke-linejoin="round" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
								</svg>
							</button>
						</Show>
					</div>
				</div>
			</Show>
			<Show when={!hasNestedComponent()}>
				<div className="flex justify-center">
					<div class="relative max-w-full overflow-auto nested-container max-h-[calc(100vh-170px)] scrollbar-thin scrollbar-thumb-gray-200 scrollbar-track-gray-50 dark:scrollbar-thumb-gray-700 dark:scrollbar-track-gray-500 overflow-x-scroll scrollbar-thumb-rounded-full scrollbar-track-rounded-full">
						<table class="table-fixed border-2 max-w-full">
							<thead class="sticky top-0 z-[1] outline outline-1 outline-gray-200 outline-offset-0 white border-0">
								<tr>
									<th class="sticky left-0 top-0 border-0 p-3 bg-white align-top outline outline-1 outline-gray-200 outline-offset-0 z-[2]"></th>
									<For each={props.component?.components[0]}>
										{(item: any, index) => {
											if (item.render !== false && !NOT_INPUT_CONTROL.includes(item.type))
												return (
													<th class={`border-2 p-3 bg-white align-top font-semiboldtext-center label-${item.dataKey.split("#")[0]} label`}>
														<div class="mb-1 min-w-[200px]">
															<div innerHTML={item.label.replace(/\$(\w+)\$/g, '').replace(/\s+/g, " ")}></div>
															<Show when={item.options}>
																<div
																	onMouseEnter={() => showInstruction(item.dataKey)}
																	onMouseLeave={() => showInstruction(null)}>
																	<button class="bg-transparent text-gray-300 rounded-full focus:outline-none h-4 w-4 align-center ml-1 hover:bg-gray-400"
																	>
																		<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
																			<path stroke-linecap="round" stroke-linejoin="round" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
																		</svg>
																	</button>
																	<Show when={instruction() === item.dataKey}>
																		<div class="font-semiboldtext-sm absolute z-index-1 w-[200px] max-h-[200px] overflow-y-auto bg-white scrollbar-thin scrollbar-thumb-gray-200 scrollbar-track-gray-50 dark:scrollbar-thumb-gray-700 dark:scrollbar-track-gray-500 overflow-x-scroll scrollbar-thumb-rounded-full scrollbar-track-rounded-full">
																			<div class="flex mt-2 p-3">
																				<p class="text-xs font-semiboldtext-left"
																					innerHTML={item.options?.map(opt => {
																						return `${opt.value} - ${cleanLabel(opt.label)}`
																					}).join("<br/>")}
																				></p>
																			</div>
																		</div>
																	</Show>
																</div>
															</Show>
														</div>
													</th>
												)
										}}
									</For>
									<Show when={isAppendable()}>
										<th class="sticky right-0 top-0 border-2 p-3 bg-white align-top outline outline-1 outline-gray-200 outline-offset-0"></th>
									</Show>
								</tr>
							</thead>
							<tbody>
								<For each={sourceAnswer()}>
									{(item: any) => {
										const dataKey = props.component.dataKey + '#' + item.value
										const position = sidebar.details.findIndex(obj => obj.dataKey === dataKey);
										const components = sidebar.details[position]?.components[0].filter((comp: any) => !NOT_INPUT_CONTROL.includes(comp.type))
										return (
											<tr>
												<td class="bg-white sticky left-0 top-0 px-4 py-2 outline outline-1 outline-gray-200 outline-offset-0 border-0">
													<div class="w-36">
														<EditInput item={item} id={item.value} readOnly={!isAppendable()} />
													</div>
												</td>
												<For each={components}
													children={(component: any, index) => {
														if (component.render !== false)
															return (
																<td class="bg-white border-2">
																	<div>
																		{FormInput({
																			onMobile: props.onMobile,
																			component,
																			index: index(),
																			config: props.config,
																			MobileUploadHandler: props.MobileUploadHandler,
																			MobileGpsHandler: props.MobileGpsHandler,
																			MobileOfflineSearch: props.MobileOfflineSearch,
																			MobileOnlineSearch: props.MobileOnlineSearch,
																			MobileOpenMap: props.MobileOpenMap,
																			setResponseMobile: props.setResponseMobile,
																			isNestedInput: true,
																		})}
																	</div>
																</td>
															)
													}
													}
												/>
												<Show when={isAppendable()}>
													<td class="bg-white sticky right-0 top-0 px-4 py-2 outline outline-1 outline-gray-200 outline-offset-0 border-0">
														<div class="col-span-2 flex justify-end p-1 space-x-1 ">
															<Show when={flag() == 1 && edited() == item.value}>
																<button class="bg-gray-500 text-white p-2 rounded-full focus:outline-none h-8 w-8 h-8 hover:bg-gray-400 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
																	disabled={disableInput()}
																	onClick={e => handleOnCancel()}>
																	<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
																		<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
																	</svg>
																</button>
															</Show>
															<button class="bg-red-600 text-white p-2 rounded-full focus:outline-none h-8 w-8 hover:bg-red-500 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
																disabled={disableInput()}
																onClick={e => handleOnDelete(Number(item.value))}>
																<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
																	<path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
																</svg>
															</button>
														</div>
													</td>
												</Show>
											</tr>
										)
									}}
								</For>
								<Show when={(flag() == 1 && edited() == 0)}>
									<tr>
										<td class="bg-white sticky left-0 top-0 px-4 py-2 outline outline-1 outline-gray-200 outline-offset-0 border-0">
											<div class="w-36">
												<AddInput />
											</div>
										</td>
										<td colSpan={props.component?.components[0]?.length}></td>
										<td class="bg-white sticky right-0 top-0 px-4 py-2 outline outline-1 outline-gray-200 outline-offset-0 border-0">
											<div class="col-span-2 flex justify-end p-1 space-x-1 ">
												<button class="bg-gray-500 text-white p-2 rounded-full focus:outline-none h-8 w-8 h-8 hover:bg-gray-400 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
													disabled={disableInput()}
													onClick={e => handleOnCancel()}>
													<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" viewBox="0 0 20 20" fill="currentColor">
														<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
													</svg>
												</button>
											</div>
										</td>
									</tr>
								</Show>
							</tbody>
						</table>
					</div>
				</div>
				<div class="flex p-4 justify-between mt-1">
					<div>
						<Show when={validationMessages().length > 0}>
							<For each={validationMessages()}>
								{(item: any) => (
									<div
										class="text-xs font-semibold">
										<div class="grid grid-cols-12"
											classList={{
												' text-orange-500 dark:text-orange-200 ': item.state == 1,
												' text-pink-600 dark:text-pink-200 ': item.state == 2,
											}} >
											<Switch>
												<Match when={item.state == 1}>
													<div class="col-span-1 flex justify-center items-start mr-1">
														<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
															<path stroke-linecap="round" stroke-linejoin="round" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
														</svg>
													</div>
												</Match>
												<Match when={item.state == 2}>
													<div class="col-span-1 flex justify-center items-start mr-1">
														<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
															<path stroke-linecap="round" stroke-linejoin="round" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
														</svg>
													</div>
												</Match>
											</Switch>
											<div class="col-span-11 text-justify mr-1" innerHTML={item.message} />
										</div>
									</div>
								)}
							</For>
						</Show>
					</div>
					<Show when={isAppendable()}>
						<div>
							<button class="bg-pink-600 text-white py-2 px-4 rounded focus:outline-blue-700 focus:outline-3 h-8 hover:bg-pink-500 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400 flex items-center"
								onKeyDown={e => handleInputKeyDown(e, props)}
								onClick={e => handleOnPlus()}>
								<div>
									<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
										<path stroke-linecap="round" stroke-linejoin="round" d="M12 4v16m8-8H4" />
									</svg>
								</div>
								<div>
									{locale.details.language[0].addRow}
								</div>
							</button>
						</div>
					</Show>
				</div>
			</Show>
			<Show when={hasNestedComponent()}>
				<div id={props.component.dataKey} ref={tabRef} data-sticky={dataSticky} class="bg-white w-[100vw]">
					<div class="flex relative flex-none min-w-full px-2 overflow-x-auto scrollbar-thin scrollbar-thumb-gray-200 scrollbar-track-gray-50 dark:scrollbar-thumb-gray-700 dark:scrollbar-track-gray-500 scrollbar-thumb-rounded-full scrollbar-track-rounded-full">
						<div class="h-10 flex mr-3 pr-3 border-r-2 border-gray-300/[.40] dark:border-gray-200/[.10]">
							<div class="my-auto">
								{props.component.label}
							</div>
						</div>
						<ul class="flex text-sm leading-6 text-slate-400">
							<For each={sourceAnswer()}>
								{(item, index) => (
									<Show when={true}>
										<li
											onClick={() => handleTabClick(index())}
											class="flex py-1 mb-1.5 px-4 rounded font-semibold space-x-2 hover:bg-blue-700 cursor-pointer"
											classList={{
												'bg-blue-800 text-white': index() === (input.currentTabIndex?.[props.component.dataKey] ?? 0)
											}}
											onMouseEnter={() => setHoveredTabIndex(index())}
											onMouseLeave={() => setHoveredTabIndex(null)}
										>
											<Show when={flag() === 1 && edited() == item.value} fallback={
												<a class="hover:text-white my-auto">
													{item.label}
												</a>
											}>
												<EditInput item={item} id={getLastId() + 1} />
											</Show>
											<Show when={isAppendable() && index() == hoveredTabIndex()}>
												<div class="w-1"></div>
												<div class="col-span-2 flex justify-end space-x-1 my-auto">
													<Show when={flag() === 0 && edited() != item.value}>
														<button class="text-white rounded-full focus:outline-none h-6 w-6 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
															disabled={disableInput()}
															onClick={e => handleOnEdit(Number(item.value))}>
															<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 m-auto" viewBox="0 0 20 20" fill="currentColor">
																<path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
															</svg>
														</button>
													</Show>
													<Show when={flag() === 1 && edited() == item.value}>
														<button class="text-white rounded-full focus:outline-none h-6 w-6 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
															disabled={disableInput()}
															onClick={e => handleOnCancel()}>
															<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 m-auto" viewBox="0 0 20 20" fill="currentColor">
																<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
															</svg>
														</button>
													</Show>
													<button class="text-white rounded-full focus:outline-none h-6 w-6  disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
														disabled={disableInput()}
														onClick={e => handleOnDelete(Number(item.value))}>
														<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 m-auto" viewBox="0 0 20 20" fill="currentColor">
															<path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
														</svg>
													</button>
												</div>
											</Show>
										</li>
									</Show>
								)}
							</For>
							<Show when={(flag() == 1 && edited() == 0)}>
								<li class="flex py-1 mb-1.5 px-4 rounded font-semibold space-x-2 w-64">
									<AddInput />
									<div class="my-auto">
										<button class="bg-gray-500 text-white rounded-full focus:outline-none h-6 w-6 hover:bg-gray-400 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
											disabled={disableInput()}
											onClick={e => handleOnCancel()}>
											<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 m-auto" viewBox="0 0 20 20" fill="currentColor">
												<path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
											</svg>
										</button>
									</div>
								</li>
							</Show>
							<Show when={isAppendable()}>
								<li class="ml-4 py-1 mb-1.5">
									<div>
										<button class="bg-pink-600 text-white py-2 px-4 rounded focus:outline-blue-700 focus:outline-3 h-8 hover:bg-pink-500 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400 flex items-center"
											onKeyDown={e => handleInputKeyDown(e, props)}
											onClick={e => handleOnPlus()}>
											<div>
												<svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 mr-2" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
													<path stroke-linecap="round" stroke-linejoin="round" d="M12 4v16m8-8H4" />
												</svg>
											</div>
											<div>
												{locale.details.language[0].addRow}
											</div>
										</button>
									</div>
								</li>
							</Show>
						</ul>
					</div>
				</div>
				<div class="flex-grow bg-white dark:bg-gray-900 overflow-y-auto mb-20">
					<For each={sourceAnswer()}>
						{(item, answerIndex) => {
							const dataKey = props.component.dataKey + '#' + item.value
							const position = sidebar.details.findIndex(obj => obj.dataKey == dataKey);
							const components = sidebar.details[position]?.components[0]
							const enabledComponents = components?.filter((comp: any) => !NOT_INPUT_CONTROL.includes(comp.type) && !(comp.disableInput == true) && comp.render !== false)
							const lastEnabledComponent = enabledComponents?.[enabledComponents.length - 1]
							return (
								<Show when={answerIndex() === (input.currentTabIndex?.[props.component.dataKey] ?? 0)}>
									<div class="space-y-3 sm:p-7 p-3">
										<For each={components}
											children={(component: any, componentIndex) => {

												const isFirstInput = components[0].dataKey == component.dataKey

												const isLastInput = components[components.length - 1]?.dataKey == component.dataKey

												const nextTabIndex = answerIndex() + 1 < sourceAnswer().length ? answerIndex() + 1 : null

												const prevTabIndex = answerIndex() > 0 ? answerIndex() - 1 : null

												return (
													<>
														<Show when={isFirstInput && prevTabIndex != null}>
															<input type="text" class="formgear-invisible-input" onFocus={() => {
																goToTab(props.component.dataKey, prevTabIndex, null, lastEnabledComponent?.dataKey, true)
															}} />
														</Show>
														<FormInput
															onMobile={props.onMobile}
															component={component}
															index={componentIndex()}
															config={props.config}
															MobileUploadHandler={props.MobileUploadHandler}
															MobileGpsHandler={props.MobileGpsHandler}
															MobileOfflineSearch={props.MobileOfflineSearch}
															MobileOnlineSearch={props.MobileOnlineSearch}
															MobileOpenMap={props.MobileOpenMap}
															setResponseMobile={props.setResponseMobile}
														></FormInput>
														<Show when={isLastInput && nextTabIndex != null}>
															<input type="text" class="formgear-invisible-input" onFocus={() => {
																goToTab(props.component.dataKey, nextTabIndex, enabledComponents[0]?.dataKey)
															}} />
														</Show>
													</>
												)
											}} />
									</div>
								</Show>

							)
						}}
					</For>
				</div>
			</Show >
			<Show when={(flag() == 2)}>
				<div class="modal-delete fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
					<div class="flex items-center justify-center min-h-screen pt-4 px-4 text-center sm:block sm:p-0">
						<div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>

						<span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

						<div class="relative inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
							<div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
								<div class="sm:flex sm:items-start">
									<div class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
										<svg class="h-6 w-6 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
											<path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
										</svg>
									</div>
									<div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
										<h3 class="text-lg leading-6 font-semibold text-gray-900" id="titleModalDelete">Deactivate account</h3>
										<div class="mt-2">
											<p class="text-sm text-gray-500" id="contentModalDelete">Are you sure you want to deactivate your account? All of your data will be permanently removed. This action cannot be undonssse.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
								<button type="button" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-semibold text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm" onClick={e => handleOnDelete(edited())}>Delete</button>
								<button type="button" class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-semibold text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm" onClick={e => handleOnCancel()}>Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</Show>
		</div >
	)
}

export default NestedInput