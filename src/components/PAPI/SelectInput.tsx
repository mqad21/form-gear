import { createEffect, createMemo, createResource, createSignal, on, onMount, Show } from "solid-js";
import Toastify from 'toastify-js';
import { TypeOption } from "../../constants";
import { FormComponentBase, Option } from "../../FormType";
import { locale } from "../../stores/LocaleStore";
import { selectOption, setSelectOption } from "../../stores/OptionStore";
import { reference } from '../../stores/ReferenceStore';
import { InputContainer, OptionSection } from "./partials";

const SelectInput: FormComponentBase = props => {

	let settedValue = (props.value) ? props.value.length > 0 ? props.value[0].value : props.value : props.value

	const [label, setLabel] = createSignal('');
	const [isLoading, setLoading] = createSignal(false);
	const isPublic = false;
	let fetched

	type optionSelect = {
		success: boolean,
		data: [],
		message: string,
	}

	const handleOnChange = (value: any, label?: any) => {
		let updatedAnswer = []
		if (label == null) {
			label = options()?.find(it => it.value == value)?.label
		}
		updatedAnswer = [{ value, label }]
		props.onValueChange([...updatedAnswer])
	}

	const getOptions = createMemo(() => {
		if (props.component.sourceOption !== undefined && props.component.typeOption === 3) {
			let newSourceOption = props.component.sourceOption.split('@');
			const componentAnswerIndex = reference.details.findIndex(obj => obj.dataKey === newSourceOption[0]);
			if ((reference.details[componentAnswerIndex].type === 21 || 22 || 23 || 26 || 27 || 29)
				|| (reference.details[componentAnswerIndex].type === 4 && reference.details[componentAnswerIndex].renderType === 2)) {
				return reference.details[componentAnswerIndex].answer
			}
		}
		return []
	})

	const options = createMemo(() => {
		return selectOption[props.component.dataKey]
		// return props.component.sourceOption !== undefined ? getOptions() : props.component.options
	})

	const hasOpenOption = createMemo(() => {
		if (!options()) return false
		return options().findIndex((opt: Option) => opt.open) !== -1
	})

	const [selectedOption, setSelectedOption] = createSignal('');

	const optionSection = () => {
		if (props.component.typeOption === TypeOption.LOOKUP) return
		return (
			<OptionSection
				{...props}
				options={options()}
				settedValue={settedValue}
				onValueChange={handleOnChange}
			/>
		)
	}

	const toastInfo = (text: string) => {
		Toastify({
			text: (text == '') ? "" : text,
			duration: 3000,
			gravity: "top",
			position: "right",
			stopOnFocus: true,
			className: "bg-pink-700/80",
			style: {
				background: "rgba(8, 145, 178, 0.7)",
				width: "400px"
			}
		}).showToast();
	}

	const parentHasLoaded = createMemo(() => {
		const parentCondition = props.component.sourceSelect?.[0]?.parentCondition

		if (!parentCondition?.length) return true

		return parentCondition.every((item: any, index) => {
			const dataKey = item.value.split('@')?.[0];
			const tobeLookup = reference.details.find(obj => obj.dataKey == dataKey)
			return tobeLookup?.answer?.length > 0
		})
	})

	const parentAnswer = createMemo(() => {
		let tempArr = []
		const params = props.component.sourceSelect

		if (params?.[0]?.parentCondition.length) {
			params[0].parentCondition.map((item: any, index) => {
				let newParams = item.value.split('@');

				let tobeLookup = reference.details.find(obj => obj.dataKey == newParams[0])
				if (tobeLookup?.answer?.length) {
					let parentValue = tobeLookup.answer[tobeLookup.answer.length - 1].value.toString()
					tempArr.push({ "key": item.key, "value": parentValue })
				}
			})
		}

		return tempArr
	})

	createEffect(() => {
		switch (props.component.typeOption) {
			case 1: {
				try {
					let options = props.component.options.map((item, value) => {
						return {
							value: item.value,
							label: item.label,
						}
					})

					let checker = props.value ? props.value != '' ? props.value[0].value : '' : ''

					createEffect(() => {
						setLabel(props.component.label)
						setSelectOption(props.component.dataKey, options)
						let ans = options.filter(val => (val.value.includes(checker)))[0] && checker != '' ? options.filter(val => (val.value.includes(checker)))[0].label : ''
						setSelectedOption(ans)
						setLoading(true)
					})
				} catch (e) {
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}

				break;
			}
			case 2: {
				if (!parentHasLoaded()) return
				try {
					if (props.config.lookupMode === 1) {
						let url
						let params
						let urlHead
						let urlParams


						if (!isPublic) {
							params = props.component.sourceSelect
							url = `${props.config.baseUrl}/${params[0].id}/filter?version=${params[0].version}`
							if (params[0].parentCondition.length > 0) {
								urlHead = url

								urlParams = params[0].parentCondition.map((item, index) => {
									let newParams = item.value.split('@');

									let tobeLookup = reference.details.find(obj => obj.dataKey == newParams[0])
									if (tobeLookup.answer) {
										if (tobeLookup.answer.length > 0) {
											let parentValue = encodeURI(tobeLookup.answer[tobeLookup.answer.length - 1].value)
											url = `${props.config.lookupKey}=${item.key}&${props.config.lookupValue}=${parentValue}`
										}
									} else {
										url = `${props.config.lookupKey}=${item.key}&${props.config.lookupValue}=''`
									}
									return url
								}).join('&')
								url = `${urlHead}&${urlParams}`

							}
						} else {
							url = `${props.config.baseUrl}`
						}

						[fetched] = createResource<optionSelect>(url, props.MobileOnlineSearch);
						let checker = props.value ? props.value != '' ? props.value[0].value : '' : ''

						createEffect(on(parentAnswer, (v, p, x) => {
							if (v && v !== p) {
								createEffect(on(fetched, (fetched) => {
									setLabel(props.component.label)
									if (fetched) {
										if (!fetched.success) {
											// toastInfo(locale.details.language[0].fetchFailed)
										} else {
											let arr

											if (!isPublic) {
												arr = []
												let cekValue = params[0].value
												let cekLabel = params[0].desc
												fetched.data.map((item, value) => {
													arr.push(
														{
															value: item[cekValue],
															label: item[cekLabel],
														}
													)
												})
											} else {
												arr = fetched.data
											}

											let ans = arr.find(obj => obj.value == checker) && checker != '' ? arr.find(obj => obj.value == checker).label : ''

											setSelectOption(props.component.dataKey, arr)
											setSelectedOption(ans)
											setLoading(true)
										}
									}
								}))
							}
						}))
					} else if (props.config.lookupMode === 2) {
						let params
						let tempArr = []

						params = props.component.sourceSelect
						let id = params[0].id
						let version = params[0].version

						if (params[0].parentCondition.length > 0) {
							params[0].parentCondition.map((item, index) => {
								let newParams = item.value.split('@');

								let tobeLookup = reference.details.find(obj => obj.dataKey == newParams[0])
								if (tobeLookup.answer) {
									if (tobeLookup.answer.length > 0) {
										let parentValue = tobeLookup.answer[tobeLookup.answer.length - 1].value.toString()
										tempArr.push({ "key": item.key, "value": parentValue })
									}
								}
							})
						}

						let getResult = (result) => {
							let arr = []

							if (result.data.length > 0) {
								let cekValue = params[0].value
								let cekLabel = params[0].desc
								let checker = props.value ? props.value != '' ? props.value[0].value : '' : ''

								result.data.map((item, value) => {
									arr.push(
										{
											value: item[cekValue],
											label: item[cekLabel],
										}
									)
								})

								let ans = arr.find(obj => obj.value == checker) && checker != '' ? arr.find(obj => obj.value == checker).label : ''
								setLabel(props.component.label)
								setSelectOption(props.component.dataKey, arr)
								setSelectedOption(ans)
								setLoading(true)
							}
						}

						fetched = props.MobileOfflineSearch(id, version, tempArr, getResult);

					}
				} catch (e) {
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}

				break;
			}
			case 3: {
				try {
					let optionsSource
					let finalOptions
					let checker = props.value ? props.value != '' ? props.value[0].value : '' : ''

					if (props.component.sourceOption !== undefined && props.component.typeOption === 3) {
						const componentAnswerIndex = reference.details.findIndex(obj => obj.dataKey === props.component.sourceOption);

						if ((reference.details[componentAnswerIndex].type === 21 || 22 || 23 || 26 || 27 || 29)
							|| (reference.details[componentAnswerIndex].type === 4 && reference.details[componentAnswerIndex].renderType === 2)) {
							optionsSource = reference.details[componentAnswerIndex].answer
							if (optionsSource != undefined) {
								finalOptions = optionsSource.filter((item, value) => item.value != 0).map((item, value) => {
									return {
										value: item.value,
										label: item.label,
									}
								})

							} else {
								finalOptions = []
							}
						}
					}
					let ans = finalOptions.find(obj => obj.value == checker) && checker != '' ? finalOptions.find(obj => obj.value == checker).label : ''

					createEffect(() => {
						setLabel(props.component.label)
						setSelectOption(props.component.dataKey, finalOptions)
						setSelectedOption(ans)
						setLoading(true)
					})

				} catch (e) {
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}

				break;
			}
			default: {
				try {
					let options
					if (props.component.options) {
						options = props.component.options.map((item, value) => {
							return {
								value: item.value,
								label: item.label,
							}
						})

					} else {
						options = []
					}

					let checker = props.value ? props.value != '' ? props.value[0].value : '' : ''

					createEffect(() => {
						setLabel(props.component.label)
						setSelectOption(props.component.dataKey, options)
						let ans = options.filter(val => (val.value.includes(checker)))[0] && checker != '' ? options.filter(val => (val.value.includes(checker)))[0].label : ''
						setSelectedOption(ans)
						setLoading(true)

					})
				} catch (e) {
					console.error(e);
					// toastInfo(locale.details.language[0].fetchFailed)
				}

				break;
			}
		}
	})

	const disabled = createMemo(() => {
		return fetched ? fetched()?.loading : false
	})

	return (
		<InputContainer {...props} optionSection={optionSection}>
			<Show when={props.component.lengthInput === undefined}>
				<input
					value={settedValue}
					placeholder=""
					onInput={(e) => {
						if (hasOpenOption())
							handleOnChange(e.currentTarget.value);
					}}
					onChange={(e) => {
						if (!hasOpenOption())
							handleOnChange(e.currentTarget.value);
					}}
					disabled={disabled()}
				/>
			</Show>
			<Show when={props.component.lengthInput !== undefined && props.component.lengthInput.length > 0}>
				<input
					value={settedValue}
					placeholder=""
					onInput={(e) => {
						if (hasOpenOption())
							handleOnChange(e.currentTarget.value);
					}}
					onChange={(e) => {
						if (!hasOpenOption())
							handleOnChange(e.currentTarget.value);
					}}
					disabled={disabled()}
					maxlength={props.component.lengthInput[0].maxlength !== undefined ? props.component.lengthInput[0].maxlength : ''}
					minlength={props.component.lengthInput[0].minlength !== undefined ? props.component.lengthInput[0].minlength : ''}
				/>
			</Show>
		</InputContainer>
	)
}
export default SelectInput;