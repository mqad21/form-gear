import { createEffect, createSignal, Switch, Match, Show, For, createMemo } from "solid-js"
import { FormComponentBase } from "../../FormType"
import Toastify from 'toastify-js'
import { locale, setLocale } from '../../stores/LocaleStore'
import { InputContainer } from "./partials"
import { handleInputFocus, handleInputKeyDown } from "../../events"

const GpsInput: FormComponentBase = props => {
  const [lat, setLat] = createSignal(0);
  const [long, setLong] = createSignal(0);


  createEffect(() => {
    const settedValue = JSON.parse(JSON.stringify(props.value))?.[0]?.value
    setLat(settedValue?.latitude)
    setLong(settedValue?.longitude)
  }, [])

  const handleLat = (e) => {
    setLat(e.target.value)
    handleChangeLocation()
  }

  const handleLong = (e) => {
    setLong(e.target.value)
    handleChangeLocation()
  }

  const handleChangeLocation = () => {
    let updatedAnswer = [];
    const source = `https://maps.google.com/maps?q=${lat()},${long()}` + `&output=embed`;
    updatedAnswer.push({ value: { 'latitude': lat(), 'longitude': long() }, label: source })
    updatedAnswer.push({ label: 'map', value: source })
    updatedAnswer.push({ label: 'latitude', value: lat() })
    updatedAnswer.push({ label: 'longitude', value: long() })
    props.onValueChange(updatedAnswer)
  }

  return (
    <InputContainer {...props}>
      <div class="grid md:grid-cols-2 gap-4">
        <div>
          <input
            classList={{
              ['formgear-input-papi-validation-' + props.classValidation]: true
            }}
            class="formgear-input-papi hide-spinner"
            type="number"
            name={props.component.dataKey + "#lat"}
            onChange={handleLat}
            onFocus={(e) => handleInputFocus(e, props)}
            onKeyDown={(e) => handleInputKeyDown(e, props)}
            placeholder={locale.details.language[0].latitude}
            value={lat()}
          />
        </div>
        <div>
          <input
            classList={{
              ['formgear-input-papi-validation-' + props.classValidation]: true
            }}
            class="formgear-input-papi hide-spinner"
            type="number"
            name={props.component.dataKey + "#long"}
            onChange={handleLong}
            onFocus={(e) => handleInputFocus(e, props)}
            onKeyDown={(e) => handleInputKeyDown(e, props)}
            placeholder={locale.details.language[0].longitude}
            value={long()}
          />
        </div>
      </div>
    </InputContainer>


  )
}

// transition ease-in-out m-0
// focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"

export default GpsInput