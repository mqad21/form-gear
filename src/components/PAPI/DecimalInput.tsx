import { createEffect, createSignal, onMount, Show } from "solid-js";
import { FormComponentBase } from "../../FormType";
import { InputContainer } from "./partials";

const DecimalInput: FormComponentBase = props => {

  const [value, setValue] = createSignal()

  let handleOnKeyup = (value: any) => {
    let decimalLength = props.component.decimalLength ?? 2
    const parsedValue = parseFloat(value).toFixed(decimalLength)
    setValue(parsedValue)
    props.onValueChange(parsedValue)
  }

  onMount(() => {
    setValue(props.value)
  })

  return (
    <>
      <Show when={props.component.lengthInput === undefined}>
        <InputContainer {...props}>
          <input value={value()}
            type="number"
            placeholder=""
            onChange={e => handleOnKeyup(e.currentTarget.value)}
          />
        </InputContainer>
      </Show>
      <Show when={props.component.lengthInput !== undefined && props.component.lengthInput.length > 0}>
        <InputContainer {...props}>
          <input
            value={value()}
            type="number"
            placeholder=""
            onChange={e => handleOnKeyup(e.currentTarget.value)}
            oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
            maxlength={props.component.lengthInput[0].maxlength !== undefined ? props.component.lengthInput[0].maxlength : ''}
            minlength={props.component.lengthInput[0].minlength !== undefined ? props.component.lengthInput[0].minlength : ''}
            max={props.component.rangeInput ? props.component.rangeInput[0].max !== undefined ? props.component.rangeInput[0].max : '' : ''}
            min={props.component.rangeInput ? props.component.rangeInput[0].min !== undefined ? props.component.rangeInput[0].min : '' : ''}
          />
        </InputContainer>
      </Show>
    </>
  )
}

export default DecimalInput