import { Component, JSXElement } from "solid-js";
import { ComponentType, Option } from "../../../FormType";
import InputContainer from "./InputContainer";
import OptionSection from "./OptionSection";

export {
    InputContainer,
    OptionSection
}

export interface InputContainerBase extends Component<{
    config: any
    component: ComponentType
    optionSection?: () => JSXElement
    classValidation?: any
    validationMessage?: any
    isNestedInput?: boolean
}> { }

export interface OptionSectionBase extends Component<{
    component: ComponentType
    options: readonly Option[]
    settedValue: any,
    onValueChange?: (value: any) => void
    value?: any
}> { }
