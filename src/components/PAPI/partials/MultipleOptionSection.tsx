import { createMemo, createSignal, For, Match, Switch } from "solid-js"
import { OptionSectionBase } from "."
import { handleInputFocus, handleInputKeyDown } from "../../../events";
import { Option } from "../../../FormType";
import { cleanLabel } from "../../../GlobalFunction";
import { reference } from "../../../stores/ReferenceStore";

const MultipleOptionSection: OptionSectionBase = props => {

	let getOptions = createMemo(() => {
        if(props.component.sourceOption !== undefined && props.component.typeOption === 3){
            let newSourceOption = props.component.sourceOption.split('@');
            const componentAnswerIndex = reference.details.findIndex(obj => obj.dataKey === newSourceOption[0]);
            if( (reference.details[componentAnswerIndex].type === 21 || 22 || 23 || 26 || 27 || 29 )
            || (reference.details[componentAnswerIndex].type === 4 && reference.details[componentAnswerIndex].renderType === 2) ){				
                return reference.details[componentAnswerIndex].answer
            }
        }
		return []
	})
	
	const [options] = createSignal<Option[]>(props.component.sourceOption !== undefined ? getOptions() : props.component.options );

    const tick = (value: string): boolean => {
        return (props.value) ? (props.value.some(d => String(d.value) === String(value)) ? true : false) : false;
    }

    const optionLabel = (value: string) => {
        let optionIndex = props.value.findIndex(d => String(d.value) === String(value))
        return props.value[optionIndex].label
    }

    const handleOpenInputChange = (value: any, label: any) => {
        let updatedAnswer = JSON.parse(JSON.stringify(props.value))
        if (props.value) {
            if (props.value.some(d => String(d.value) === String(value))) {
                let valueIndex = options().findIndex((item) => item.value == value);
                updatedAnswer = updatedAnswer.filter((item) => item.value != value)
                if (options()[valueIndex].label !== label) updatedAnswer.push({ value: value, label: label, open: true })
            } else {
                updatedAnswer.splice(updatedAnswer.length, 0, { value: value, label: label })
            }
        } else {
            updatedAnswer = [];
            updatedAnswer.push({ value: value, label: label })
        }
        props.onValueChange(updatedAnswer);
    }


    return (
        <div class="grid font-semiboldtext-sm content-start"
            classList={{
                'grid-cols-1': props.component.cols === 1 || props.component.cols === undefined,
                'grid-cols-2': props.component.cols === 2,
                'grid-cols-3': props.component.cols === 3,
                'grid-cols-4': props.component.cols === 4,
                'grid-cols-5': props.component.cols === 5,
            }}
        >
            <For each={props.options}>
                {(item, index) => {
                    let label = item.label ? ` - ${cleanLabel(item.label)}` : ''
                    label = `${Math.pow(2, index())}${label}`
                    return (
                        <Switch>
                            <Match when={(item.open) && (tick(item.value))}>
                                <div class="font-semiboldtext-sm space-x-2 py-2.5 px-4 grid grid-cols-12">
                                    <div class="col-span-2">
                                        <label class="cursor-pointer text-sm" for={"chexbox" + index()}>
                                            <input class="form-check-input appearance-none h-4 w-4 border 
                                                        border-gray-300 rounded-sm bg-white 
                                                        checked:bg-blue-600 checked:border-blue-600 
                                                        focus:outline-none transition duration-200 align-top 
                                                        bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer
                                                        checked:disabled:bg-gray-500 checked:dark:disabled:bg-gray-300 
                                                        disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400 relative"
                                                type="checkbox"
                                                disabled
                                                value={item.value}
                                                checked={(item.value) ? tick(item.value) : false} id={"checkbox-" + props.component.dataKey + "-" + index()}
                                            />
                                        </label>
                                    </div>
                                    <div class="col-span-10">
                                        <input
                                            onKeyDown={(e) => handleInputKeyDown(e, props)}
                                            onFocus={(e) => handleInputFocus(e, props)}
                                            value={optionLabel(item.value)}
                                            name={props.component.dataKey + "-opt" + index()}
                                            class="w-full
                                                        font-semibold
                                                        px-4
                                                        py-2.5
                                                        text-sm
                                                        text-gray-700
                                                        bg-white bg-clip-padding
                                                        border border-solid border-gray-300
                                                        rounded
                                                        transition
                                                        ease-in-out
                                                        m-0
                                                        focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                                            onChange={e => handleOpenInputChange(item.value, e.currentTarget.value)}
                                        />
                                    </div>
                                </div>
                            </Match>
                            <Match when={!(item.open) || !(tick(item.value))}>
                                <div class="font-semiboldtext-sm space-x-2 py-2.5 px-4 grid grid-cols-12">
                                    <div class="col-span-2">
                                        <label class="cursor-pointer text-sm">
                                            <input class="form-check-input appearance-none h-4 w-4 border 
                                                            border-gray-300 rounded-sm bg-white 
                                                            checked:bg-blue-600 checked:border-blue-600 
                                                            focus:outline-none transition duration-200 mt-1 align-top 
                                                            bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer
                                                            checked:disabled:bg-gray-500 checked:dark:disabled:bg-gray-300 
                                                            disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400 relative"
                                                type="checkbox"
                                                disabled
                                                value={item.value}
                                                checked={(item.value) ? tick(item.value) : false} id={"checkbox-" + props.component.dataKey + "-" + index()} />
                                        </label>
                                    </div>
                                    <div class="col-span-10" innerHTML={label}></div>
                                </div>
                            </Match>
                        </Switch>
                    )
                }}
            </For>
        </div>
    )
}

export default MultipleOptionSection