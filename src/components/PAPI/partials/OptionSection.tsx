import { For, Match, Switch } from "solid-js"
import { OptionSectionBase } from "."
import { handleInputFocus, handleInputKeyDown } from "../../../events"
import { cleanLabel } from "../../../GlobalFunction"

const OptionSection: OptionSectionBase = props => {

    const handleOpenInputChange = (value: any, label: any) => {
        let updatedAnswer = JSON.parse(JSON.stringify(props.value))
        if (props.value) {
            if (props.value.some(d => String(d.value) === String(value))) {
                let valueIndex = props.options.findIndex((item) => item.value == value);
                updatedAnswer = updatedAnswer.filter((item) => item.value != value)
                if (props.options[valueIndex].label !== label) updatedAnswer.push({ value: value, label: label, open: true })
            } else {
                updatedAnswer.splice(updatedAnswer.length, 0, { value: value, label: label })
            }
        } else {
            updatedAnswer = [];
            updatedAnswer.push({ value: value, label: label })
        }
        props.onValueChange(updatedAnswer);
    }
    
    return (
        <div class="grid font-semiboldtext-sm content-start"
            classList={{
                'grid-cols-1': props.component.cols === 1 || props.component.cols === undefined,
                'grid-cols-2': props.component.cols === 2,
                'grid-cols-3': props.component.cols === 3,
                'grid-cols-4': props.component.cols === 4,
                'grid-cols-5': props.component.cols === 5,
            }}
        >
            <For each={props.options}>
                {(item, index) => {
                    let label = item.label ? ` - ${cleanLabel(item.label)}` : ''
                    label = `${item.value}${label}`
                    return (
                        <div class="font-semiboldtext-sm space-x-2 py-2.5 px-4 grid grid-cols-12">
                            <div class="col-span-2 text-center">
                                <label class="cursor-pointer text-sm" for={props.component.dataKey + index()}>
                                    <input type="radio" checked={props.settedValue === item.value}
                                        class="checked:disabled:bg-gray-500 checked:dark:disabled:bg-gray-300 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
                                        value={item.value}
                                        disabled />
                                </label>
                            </div>
                            <Switch>
                                <Match when={(item.open) && (props.settedValue === item.value)}>
                                    <div class="col-span-10">
                                        <input value={(props.value) ? props.value.length > 0 ? props.value[0].label : item.label : item.label}
                                            class="w-full font-semiboldpx-4 py-2.5 text-sm text-gray-700 bg-white bg-clip-padding
                                            border border-solid border-gray-300 rounded transition ease-in-out m-0
                                            focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none
                                            disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
                                            disabled
                                            name={props.component.dataKey + "-opt" + index()}
                                            onKeyDown={e => handleInputKeyDown(e, props)}
                                            onFocus={e => handleInputFocus(e, props)}
                                            onInput={e => handleOpenInputChange(item.value, e.currentTarget.value)}
                                        />
                                    </div>
                                </Match>
                                <Match when={!(item.open) || (props.settedValue !== item.value)}>
                                    <div class="col-span-10" innerHTML={label}></div>
                                </Match>
                            </Switch>
                        </div>
                    )
                }
                }
            </For>
        </div>
    )
}

export default OptionSection