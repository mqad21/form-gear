import { FormComponentBase } from "../../FormType"
import { createSignal, Show, For, Switch, Match } from 'solid-js'
import { InputContainer } from "./partials"
import { handleInputFocus, handleInputKeyDown } from "../../events"

const TextInput: FormComponentBase = props => {

  return (
    <>
      <Show when={props.component.lengthInput === undefined}>
        <InputContainer {...props}>
          <input
            value={props.value}
            placeholder=""
            onChange={(e) => {
              props.onValueChange(e.currentTarget.value);
            }}
          />
        </InputContainer>

      </Show>
      <Show when={props.component.lengthInput !== undefined && props.component.lengthInput.length > 0}>
        <InputContainer {...props}>
          <input
            value={props.value}
            placeholder=""
            onChange={(e) => {
              props.onValueChange(e.currentTarget.value);
            }}
            maxlength={props.component.lengthInput[0].maxlength !== undefined ? props.component.lengthInput[0].maxlength : ''}
            minlength={props.component.lengthInput[0].minlength !== undefined ? props.component.lengthInput[0].minlength : ''}
          />
        </InputContainer>
      </Show>
    </>
  )
}

export default TextInput