import { createSignal, For, Match, Show, Switch } from "solid-js"
import { handleInputFocus, handleInputKeyDown } from "../../events"
import { FormComponentBase } from "../../FormType"
import { InputContainer } from "./partials"

const EmailInput: FormComponentBase = props => {

  return (
    <InputContainer {...props}>
      <input value={props.value}
        type="email"
        placeholder=""
        onChange={(e) => {
          props.onValueChange(e.currentTarget.value);
        }}
      />
    </InputContainer>
  )
}

export default EmailInput