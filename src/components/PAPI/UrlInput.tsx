import { createSignal, For, Match, Show, Switch } from "solid-js"
import { FormComponentBase } from "../../FormType"
import { InputContainer } from "./partials"

const UrlInput: FormComponentBase = props => {
  const config = props.config
  const [disableInput] = createSignal((config.formMode > 1) ? true : props.component.disableInput)

  return (
    <InputContainer {...props}>
      <input
        value={props.value}
        type="url"
        placeholder=""
        onChange={(e) => {
          props.onValueChange(e.currentTarget.value);
        }}
      />
    </InputContainer>
  )
}

export default UrlInput