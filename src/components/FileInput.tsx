import mime from "mime-types";
import { createEffect, createSignal, For, Match, Show, Switch } from "solid-js";
import { FormComponentBase } from "../FormType";
import { toastInfo } from "../GlobalFunction";
import { locale } from "../stores/LocaleStore";
import { setMedia } from "../stores/MediaStore";

const FileInput: FormComponentBase = props => {
  const [file, setFile] = createSignal<File>()
  const [fileSource, setFileSource] = createSignal('');
  let reader = new FileReader();

  const config = props.config
  const [disableInput] = createSignal((config.formMode > 1) ? true : props.component.disableInput)

  createEffect(async () => {
    if (props.value[0]) {
      let imgSrc = props.value[0].value
      setFileSource(imgSrc)

      const xhr = new XMLHttpRequest();
      xhr.responseType = 'blob'

      xhr.open('GET', imgSrc, true);

      xhr.onload = function (e) {
        if (this.status == 200) {
          let fileName = imgSrc.substring(imgSrc.lastIndexOf('/') + 1);
          const extension = (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined; /[^.]+$/.exec(fileName);
          if (!extension) fileName += `.${mime.extension(this.response.type)}`
          const file = new File([this.response], fileName, { type: this.response.type })
          setFile(file)
        }
      };

      xhr.send();
    }
  })

  let handleOnChange = (event) => {
    var event = JSON.parse(event);
    let updatedAnswer = JSON.parse(JSON.stringify(props.value))
    updatedAnswer = [];

    updatedAnswer.push({ value: event.image, label: event.label, type: event.type })

    props.onValueChange(updatedAnswer)
  }

  let setValue = (data) => {
    handleOnChange(data)
  }

  let clickUpload = () => {
    props.MobileUploadHandler(setValue)
  }

  let hideModal = () => {
    let component = document.querySelector("#popup-modal" + props.component.dataKey);
    component.classList.toggle("flex");
    component.classList.toggle("hidden");
  }

  let getFileContent = (data) => {
    let updatedAnswer = JSON.parse(JSON.stringify(props.value))

    if (data.target.files && data.target.files[0]) {
      let doc = data.target.files[0];
      setFile(doc)
      reader.readAsDataURL(doc)
      reader.onload = e => {
        const fileName = doc.name
        const fileType = doc.type
        updatedAnswer = [];

        let urlImg = URL.createObjectURL(doc)

        updatedAnswer.push({ value: urlImg, label: fileName, type: fileType })
        setMedia("details", "media", props.component.dataKey + "#" as keyof Map<string, File>, doc)

        props.onValueChange(updatedAnswer)
        toastInfo(locale.details.language[0].fileUploaded, 1000)
      }
    }

  }

  const download = () => {
    const a = window.document.createElement('a');
    a.href = window.URL.createObjectURL(file());
    a.download = file().name;
    a.click()
  }

  const handleOnDelete = () => {
    setMedia("details", "media", props.component.dataKey as keyof Map<string, File>, undefined)
    props.onValueChange([])
  }

  const [instruction, setInstruction] = createSignal(false);
  const showInstruction = () => {
    (instruction()) ? setInstruction(false) : setInstruction(true);
  }

  const [enableRemark] = createSignal(props.component.enableRemark !== undefined ? props.component.enableRemark : true);
  const [disableClickRemark] = createSignal((config.formMode > 2 && props.comments == 0) ? true : false);

  return (
    <div>
      <div class="md:grid md:grid-cols-3 border-b border-gray-300/[.40] dark:border-gray-200/[.10] p-2">

        <div class="font-semiboldtext-sm space-y-2 py-2.5 px-2 md:col-span-1">
          <div class="inline-flex space-x-2">
            <div innerHTML={props.component.label} />
            <Show when={props.component.required}>
              <span class="text-pink-600">*</span>
            </Show>
            <Show when={props.component.hint}>
              <button class="bg-transparent text-gray-300 rounded-full focus:outline-none h-4 w-4 hover:bg-gray-400 hover:text-white flex justify-center items-center"
                onClick={showInstruction}>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M13 16h-1v-4h-1m1-4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                </svg>
              </button>
            </Show>
          </div>
          <div class="flex mt-2">
            <Show when={instruction()}>
              <div class="italic text-xs font-extralight text-zinc-400 " innerHTML={props.component.hint} />
            </Show>
          </div>
        </div>

        <div class="font-semiboldtext-sm space-x-2 py-2.5 px-2 md:col-span-2 items-center grid grid-cols-12">
          <div class="col-span-8">
            <Show when={file() !== undefined}>
              <div class="flex items-center">
                <div onClick={download} class=" cursor-pointer bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-4 py-3.5 rounded dark:bg-blue-200 dark:text-blue-800">{file().name}
                </div>
                <button class="bg-red-600 text-white p-2 rounded-full focus:outline-none hover:bg-red-500 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
                  disabled={disableInput()}
                  onClick={handleOnDelete}>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
                  </svg>
                </button>
              </div>
            </Show>
          </div>
          <div class="col-span-4 flex justify-end">
            <Switch>
              <Match when={config.clientMode == 2} >
                <button class="bg-white text-gray-500 p-2 mr-2 rounded-full focus:outline-none h-10 w-10 hover:bg-pink-200 hover:text-pink-400 hover:border-pink-200 border-2 border-gray-300  disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
                  disabled={disableInput()}
                  onClick={() => clickUpload()}>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z" />
                  </svg>
                </button>
              </Match>
              <Match when={config.clientMode == 1}>
                <input type="file" onchange={(e) => { getFileContent(e) }} accept={props.component.accept} id={"inputFile_" + props.component.dataKey} style="color: transparent;"
                  class="hidden w-full text-sm text-slate-500 file:mr-4 file:py-2 file:px-4 file:rounded-full file:border-0 file:text-sm file:font-semibold file:bg-violet-50 file:text-violet-700 hover:file:bg-violet-100"
                  name={props.component.dataKey} />
                <button class="bg-white text-gray-500 p-2 mr-2 rounded-full focus:outline-none h-10 w-10 hover:bg-pink-200 hover:text-pink-400 hover:border-pink-200 border-2 border-gray-300  disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
                  disabled={disableInput()}
                  onClick={e => { (document.getElementById("inputFile_" + props.component.dataKey) as HTMLInputElement).click() }} title={locale.details.language[0].uploadFile}>
                  <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 14.25v-2.625a3.375 3.375 0 00-3.375-3.375h-1.5A1.125 1.125 0 0113.5 7.125v-1.5a3.375 3.375 0 00-3.375-3.375H8.25m2.25 0H5.625c-.621 0-1.125.504-1.125 1.125v17.25c0 .621.504 1.125 1.125 1.125h12.75c.621 0 1.125-.504 1.125-1.125V11.25a9 9 0 00-9-9z" />
                  </svg>
                </button>
              </Match>
            </Switch>

            <Show when={enableRemark()}>
              <button class="relative inline-block bg-white p-2 h-10 w-10 text-gray-500 rounded-full  hover:bg-yellow-100 hover:text-yellow-400 hover:border-yellow-100 border-2 border-gray-300 disabled:bg-gray-200 dark:disabled:bg-gray-700 dark:disabled:text-gray-400"
                disabled={disableClickRemark()}
                onClick={e => props.openRemark(props.component.dataKey)}>
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                  <path stroke-linecap="round" stroke-linejoin="round" d="M7 8h10M7 12h4m1 8l-4-4H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-3l-4 4z" />
                </svg>
                <span class="absolute top-0 right-0 inline-flex items-center justify-center h-6 w-6
                    text-xs font-semibold text-white transform translate-x-1/2 -translate-y-1/4 bg-pink-600/80 rounded-full"
                  classList={{
                    'hidden': props.comments === 0
                  }}>
                  {props.comments}
                </span>
              </button>
            </Show>
          </div>
        </div>

        <div class="col-span-12"
          classList={{
            ' border-b border-orange-500 pb-3 ': props.classValidation === 1,
            ' border-b border-pink-600 pb-3 ': props.classValidation === 2,
          }}>
        </div>
        <div class="col-span-12 pb-4">
          <Show when={props.validationMessage.length > 0}>
            <For each={props.validationMessage}>
              {(item: any) => (
                <div
                  class="text-xs font-semiboldmt-1">
                  <div class="grid grid-cols-12"
                    classList={{
                      ' text-orange-500 dark:text-orange-200 ': props.classValidation === 1,
                      ' text-pink-600 dark:text-pink-200 ': props.classValidation === 2,
                    }} >
                    <Switch>
                      <Match when={props.classValidation === 1}>
                        <div class="col-span-1 flex justify-center items-start">
                          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                          </svg>
                        </div>
                      </Match>
                      <Match when={props.classValidation === 2}>
                        <div class="col-span-1 flex justify-center items-start">
                          <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                          </svg>
                        </div>
                      </Match>
                    </Switch>
                    <div class="col-span-11 text-justify mr-1" innerHTML={item} />
                  </div>
                </div>
              )}
            </For>
          </Show>
        </div>

      </div>
    </div>

  )
}

// transition ease-in-out m-0
// focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"

export default FileInput