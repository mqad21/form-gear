import { NestedType } from "../constants"
import { ControlType, OPTION_CONTROL } from "../FormType"
import { getContainer, refocusLastSelector } from "../GlobalFunction"
import { setInput } from "../stores/InputStore"

const enabledSelector = "input:not(:disabled):not(.formgear-select-unit .solid-select-input),textarea:not(.hidden-input):not(:disabled)"

const navigateInput = (e: any, props: any, isBackward: boolean = false) => {
    /** Handle nested input identifier */
    if (e.target.classList.contains("formgear-nested-identifier")) {
        e.target.blur()
        return
    }

    e.preventDefault();

    const inputs =
        Array.prototype.slice.call(getContainer()?.querySelectorAll(enabledSelector))
    const activeElementIndex = inputs.indexOf(document.activeElement)
    let index = activeElementIndex

    if (isBackward) {
        index--
        if ((activeElementIndex) === 0) {
            document.getElementById("formgear-prev")?.click()
            return;
        }
    } else {
        index++
        if ((activeElementIndex + 1) === inputs.length) {
            document.getElementById("formgear-next")?.click()
            return;
        }
    }

    const input = inputs[index]
    input?.focus()

    if (OPTION_CONTROL.includes(props.component.dataKey)) {
        input?.select()
    }

}

const moveCaret = (e: any, charCount: number, isReverse: boolean = false) => {
    try {
        let [start, end] = [e.target.selectionStart ?? 0, e.target.selectionEnd ?? e.target.value.length - 1]
        if (isReverse) {
            if (start > 0) start--
            if (end > 0) end--
        } else {
            if (start <= e.target.value.length) start++
            if (end <= e.target.value.length) end++
        }
        e.target.setSelectionRange(start, end)
    } catch (e) {

    }
}

export const handleInputKeyDown = (e: any, props: any) => {
    handleEnterPress(e, props)
    handleTabPress(e, props)
    handleRightArrowPress(e, props)
    handleLeftArrowPress(e, props)
    handleUpArrowPress(e, props)
    handleDownArrowPress(e, props)
}

export const handleEnterPress = (e: any, props: any) => {
    if (e.shiftKey) {
        e.stopPropagation()
        return;
    }

    if (e.keyCode === 13) {
        navigateInput(e, props)
    }
}

export const handleTabPress = (e: any, props: any) => {
    if (e.keyCode === 9) {
        if (e.shiftKey) {
            navigateInput(e, props, true)
        } else {
            navigateInput(e, props)
        }
    }

}

export const handleRightArrowPress = (e: any, props: any) => {
    if (e.keyCode === 39) {
        if (e.altKey) {
            moveCaret(e, 1)
        } else if (props.isNestedInput && props.component.nestedType !== NestedType.COLUMN) {
            navigateInput(e, props)
        }
    }
}

export const handleLeftArrowPress = (e: any, props: any) => {
    if (e.keyCode === 37) {
        if (e.altKey) {
            moveCaret(e, 1, true)
        } else if (props.isNestedInput && props.component.nestedType !== NestedType.COLUMN)
            navigateInput(e, props, true)
    }
}

export const handleUpArrowPress = (e: any, props: any) => {
    if (e.keyCode === 38) {
        const splittedDataKey = e.target.getAttribute("name").split("#")
        const targetId = parseInt(splittedDataKey[splittedDataKey.length - 1]) - 1
        splittedDataKey[splittedDataKey.length - 1] = targetId
        if (targetId > 0) {
            e.preventDefault()
            setInput("currentDataKey", splittedDataKey.join("#"))
            refocusLastSelector()
        }
    }
}

export const handleDownArrowPress = (e: any, props: any) => {
    if (e.keyCode === 40) {
        const splittedDataKey = e.target.getAttribute("name").split("#")
        const targetId = parseInt(splittedDataKey[splittedDataKey.length - 1]) + 1
        splittedDataKey[splittedDataKey.length - 1] = targetId
        if (targetId > 0) {
            e.preventDefault()
            setInput("currentDataKey", splittedDataKey.join("#"))
            refocusLastSelector()
        }
    }
}