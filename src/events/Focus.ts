import { ClientMode } from "../constants"
import { OPTION_CONTROL } from "../FormType"
import { scrollCenterInput } from "../GlobalFunction"
import { setInput } from "../stores/InputStore"

export const handleInputFocus = (e: any, props: any) => {
    if (props.config.clientMode == ClientMode.PAPI) {
        // const elem = props.isNestedInput ? e.target.offsetParent : e.target
        const elem = e.target
        const scrollContainer = props.isNestedInput ? document.querySelector(".nested-container") as HTMLElement : null
        setInput("currentDataKey", elem?.getAttribute("name"))
        scrollCenterInput(elem, scrollContainer)
        document.querySelectorAll(".label").forEach(el => el.classList.remove('font-semibold'))
        document.querySelectorAll(`.label-${props.component.dataKey.split("#")[0]}`).forEach(el => el.classList.add('font-semibold'))
        
        if (OPTION_CONTROL.includes(props.component.type)) {
            elem?.select()
        }
    }
}

export const handleInputBlur = (e: any, props: any) => {
    if (props.config.clientMode == ClientMode.PAPI) {
        document.querySelectorAll(".label").forEach(el => el.classList.remove('font-semibold'))
    }
}