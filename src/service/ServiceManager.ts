import {ServiceResponse, ServiceConfig, IServiceManager} from "./Types";
import { ServiceMethodConstants as constantMethod } from "../constants";

abstract class ServiceManager implements IServiceManager {

    constructor(public _config: ServiceConfig){
    }

    abstract Request<T>(url: string): Promise<ServiceResponse<T>> 
}

class OnlineFetch extends ServiceManager {
    private _baseUrl: string

    constructor() {
        super({})
        this._baseUrl = constantMethod.BASEURL
    }

    async Request<T>(url: string): Promise<ServiceResponse<T>> {
        return fetch(`${this._baseUrl}${url}`, this._config).then((response) =>  response).then((response) => response.json().then((result) => {return <ServiceResponse<T>>{status: response.status, result}}))
    }
}

class OfflineFetch extends ServiceManager {
    constructor(){
        super({            
            method: 'GET',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
        })
    }

    async Request<T>(url: string): Promise<ServiceResponse<T>> {
        return fetch(url, this._config).then((response) =>  response).then((response) => response.json().then((result) => {return <ServiceResponse<T>>{status: response.status, result}}))
    }
}

export let onlineLookup =  new OnlineFetch()
export let offlineLookup = new OfflineFetch()
