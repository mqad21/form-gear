export type ServiceError = {
    code: string,
    message: string,
};

export type LookupResponse = {
    data: Record<string | number, unknown>[],
    message: string,
    success: boolean
}

export type ServiceResponse<T> = {
    result: T & LookupResponse & ServiceError,
    status: number | boolean
}

export type ServiceConfig = RequestInit

export interface IServiceManager {
    _config: ServiceConfig
    Request<T>(url: string): Promise<ServiceResponse<T>>
}