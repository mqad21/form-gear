export enum ClientMode {
    CAWI = 1,
    CAPI = 2,
    PAPI = 3
}

export enum FormMode {
    OPEN    = 1,
    REVIEW  = 2,
    CLOSE   = 3
}

export enum TypeOption {
    TEMPLATE    = 1,
    LOOKUP      = 2,
    VARIABLE    = 3
}

export enum NestedType {
    COLUMN      = 1,
    GRID        = 2,
}

export enum ServiceMethodConstants {
    BASEURL = `https://fasih-survey.bps.go.id/lookup/api/v1/collections/`,
    LOOKUPKEY = `keys`, //optional
    LOOKUPVALUE = `values`, //optional
    GET = `GET`,
    POST = `POST`
}