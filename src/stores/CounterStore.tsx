import { createStore } from "solid-js/store";

export type Counter = {
    render: number,
    validate: number,
    renderProgress: number | string,
    renderState: string
}

export const [counter, setCounter] = createStore<Counter>({
    render: 0,
    validate: 0,
    renderProgress: 0,
    renderState: ''
});

export let [percentage, setPercentage] = createStore({ number: 0 })