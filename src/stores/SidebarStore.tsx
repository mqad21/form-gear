import { createStore } from "solid-js/store";
import { Component } from "./TemplateStore";

export type Detail = {
    dataKey: string
    name: string
    label: string
    description: string
    level: number,
    index: number[],
    components: Component,
    sourceQuestion?: string,
    enable: boolean,
    enableCondition: string,
    componentEnable: string[]
}

export interface Sidebar {
    details: Detail[],
    sorted?: Detail[]
}

export const [sidebar, setSidebar] = createStore<Sidebar>({
    details: [],
    get sorted() {
        const sortedSidebar = []
        this.details.forEach(s0 => {
            if (s0.level != 0 || !s0.enable) return
            sortedSidebar.push(s0)
            sidebar.details.forEach(s1 => {
                if (s1.level != 1 || !s1.enable || s1.index[1] != s0.index[1]) return
                sortedSidebar.push(s1)
                sidebar.details.forEach(s2 => {
                    if (s2.level != 2 || !s2.enable || s0.index[1] != s1.index[1] || s1.index[1] != s2.index[1]
                        || s1.index[3] != s2.index[3] || s1.index[4] != s2.index[4]) return
                    sortedSidebar.push(s2)
                    sidebar.details.forEach(s3 => {
                        if (s3.level != 3 || !s3.enable || s0.index[1] != s1.index[1]
                            || s1.index[1] != s2.index[1]
                            || s1.index[3] != s2.index[3]
                            || s2.index[5] != s3.index[5]
                            || s2.index[6] != s3.index[6]) return
                        sortedSidebar.push(s3)
                    })
                })
            })
        })
        return sortedSidebar
    }
});