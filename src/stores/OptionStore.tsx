import { createStore } from "solid-js/store";
import { Option } from "../FormType";

export const [selectOption, setSelectOption] = createStore<{ [key: string]: Option[] }>({});