import { createStore } from "solid-js/store";

type Input = {
    firstDataKey: string
    currentDataKey: string
    currentTabIndex: { [k: string]: number },
    nestedTabOffsetTop: { [k: string]: number }
}

export const [input, setInput] = createStore<Input>({
    firstDataKey: "",
    currentDataKey: "",
    currentTabIndex: null,
    nestedTabOffsetTop: null,
});