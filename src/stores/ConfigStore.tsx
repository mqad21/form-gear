import { createStore } from "solid-js/store";

type Config = {
    clientMode: number
    token: string
    baseUrl: string
    lookupKey: string
    lookupValue: string
    lookupMode: number
    username: string
    formMode: number
    initialMode: number
    principalCollection: Record<string, string|number>[]
}

export const [configStore, setConfigStore] = createStore<Config>({
    clientMode: 1,
    token: '',
    baseUrl: '',
    lookupKey: 'keys',
    lookupValue: 'values',
    lookupMode: 1,
    username: 'AdityaSetyadi',
    formMode: 1,
    initialMode: 2,
    principalCollection: []
});